\documentclass{article}
% General document formatting
\usepackage[margin=0.7in]{geometry}
\usepackage[parfill]{parskip}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb}
\usepackage{url}
\usepackage{graphicx}
% subfigures
\usepackage{caption}
\usepackage{subcaption}

    % Related to math
    %\usepackage{amsmath,amssymb,amsfonts,amsthm}
%%%%%%%% Author and Title %%%%%%%%%%%%%%%%%%%%%%%
\usepackage{authblk}
\title{\textbf{Turbulent thermalisation}\\Final project for the lecture "Quantum particles\\and fields on the lattice" at UiS (FYS902)}
\author[]{Gerhard Ungersbaeck}
%\affil[1]{TeX.SX}
%\date{}                     %% if you don't need date to appear
\setcounter{Maxaffil}{0}
\renewcommand\Affilfont{\itshape\small}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\maketitle
\tableofcontents
\section{Introduction}
The aim of this project is the application of the acquired knowledge during the "Quantum particles and
fields on the lattice" lecture held by Prof. Alexander Rothkopf at the University of Stavanger.
For this purpose, the evolution of quantum fluctuation in a linear sigma model is simulated on the lattice.
This is based on a publication concerning reheating \cite{micha_turbulent_2004}, where the evolving
fluctuations of the inflaton field are studied to explain particle creation after inflation.
The initial conditions for the simulation are quantum fluctuations and a nonzero expectation value of one
inflaton field
component. The field is evolved classically, and at each time step the particle number per mode is calculated 
based on an Ansatz for the form of the two-point function for the interacting theory.
This approach is referred to as semi-classical simulation.
The theory is introduced in sec.~\ref{sec:Theory}, revising the results for the free theory and 
explaining the aforementioned Ansatz. Sec.~\ref{sec:Numerics} discusses the numerical methods and implementation.
Sec.~\ref{sec:Results} summarizes the obtained results, and Sec.~\ref{sec:Concl} concludes this project.

\section{Theory}\label{sec:Theory}

\subsection{Linear sigma model}\label{ssec:sigma}
The Lagrangian (-density) of the linear sigma model with global $O(N)$ symmetry is given by
\begin{equation}
    \mathcal{L} = \frac{1}{2} \partial_{\mu} \phi^a \partial^{\mu} \phi^a - \frac{M^2}{2} \phi^a \phi^a -
\frac{\lambda}{4} (\phi^a \phi^a)^2.
\end{equation}
The mass term is the most general one, since a term of the form $A^{ab} \phi^a \phi^b$ can be diagonalized with an
$O(N)$ transformation, which leaves the interaction term invariant.
The Hamiltonian (-density) is given by
\begin{align}
    \mathcal{H} &= \pi^a \dot{\phi^a} - \mathcal{L}, \quad 
    \text{where} \quad \pi^a = \frac{\delta \mathcal{L}}{\delta \dot{\phi}} \quad \text{is used to get} \quad \dot{\phi}^a = \pi^{a} \\
    &= \frac{1}{2} \pi^a \pi^a - \frac{1}{2} \partial_{i} \phi^a \partial^{i} \phi^a + \frac{M^2}{2} \phi^a \phi^a + \frac{\lambda}{4} (\phi^a \phi^a)^2 = \\
    &= \frac{1}{2} \pi^a \pi^a + \frac{1}{2} \nabla \phi^a \nabla \phi^a  + \frac{M^2}{2} \phi^a \phi^a + \frac{\lambda}{4} (\phi^a \phi^a)^2.
\end{align}
The equations of motions (e.o.m.) in the Hamilton formalism are given by
\begin{align}
    \label{eqn:eom}
    \dot{\phi^a} &= \frac{\delta \mathcal{H}}{\delta \pi^a} = \pi^a, \\
    \dot{\pi^a} &= -\frac{\delta \mathcal{H}}{\delta \phi^a} =
    - \Big( -\nabla^2 + M^2 + \lambda (\phi^b \phi^b) \Big) \phi^a.
\end{align}

\subsection{Free theory}\label{ssec:freeTheory}
The free theory, without the interaction term $\propto \lambda$, can be quantized by solving
the e.o.m. and promoting the dynamical variables to operators \cite{Peskin:1995ev}.
The e.o.m. in Fourier space are given by
\begin{equation}
    \big( \frac{\partial^2}{\partial_t^2} + (|p| + M^2) \big) \tilde{\phi} (p,t) = 0.
\end{equation}
This is also the e.o.m. for a harmonic oscillator with frequency
\begin{equation}
    \omega_p = \sqrt{ |p| + M^2}.
\end{equation}
The solution of the quantized theory is therefore given by 
\begin{equation}
\phi(x) = \int \frac{d^3p}{(2 \pi)^d} \frac{1}{\sqrt{2 \omega_p}} (a_p  e^{i p x} + a^\dagger_p  e^{-i p x})
\end{equation}
where 
\begin{equation}
    [a_p,a^\dagger_{p^\prime}] = (2 \pi)^3 \delta(p - p^\prime)
\end{equation}
Incorporating the time dependence of the ladder operator yields the expression 
\begin{equation}
\phi(x,t) = \int \frac{d^3p}{(2 \pi)^d} \frac{1}{\sqrt{2 \omega_p}} (a_p  e^{-i(\omega_p t - p x)} + a^\dagger_p  e^{i (\omega_p t - p x)})
\end{equation}
The two-point function in vacuum is given by\\
\begin{equation}
    \langle \phi(x, t) \phi(y, t^\prime) \rangle =
    \int \frac{d^3p}{(2 \pi)^d} \frac{1}{2 \omega_p} e^{i(\omega_p (t-t^\prime) -p (x-y))}
\end{equation}
The two point function in medium is given by
\begin{equation}
    \label{equ:twopoint}
    \langle \phi(x, t) \phi(y, t^\prime) \rangle =
    \int \frac{d^3p}{(2 \pi)^d} \frac{n_p + 1/2}{\omega_p} e^{i(\omega_p (t-t^\prime) -p (x-y))}
\end{equation}
where $n_p = a^\dagger_p a_p$ is the number of particles in mode $p$.

The propagator of the real scalar field, defined as the time-ordered two-point functions
can be split in a statistical and spectral component \cite{berges_introduction_2004}: 
\begin{equation}
    G(x,t,y,t^\prime) = F(x,t,y,t^\prime) - \frac{i}{2} \rho (x,t,y,t^\prime) 
    \text{sign}_\mathcal{C} (t-t^\prime)
\end{equation}
The first contains information about the particle distribution function or equivalently
the particle number per mode. The latter encodes the spectrum of the theory.
For the scalar field theory it is given by the equal time commutation relation.
The statistical component is given by
\begin{equation}
    F(x,t,y,t^\prime) = \frac{1}{2} \langle \{\phi(x, t) \phi(y, t^\prime)\} \rangle.
\end{equation}
For the scalar field field theory this simplifies to
\begin{equation}
    F(x,t,y,t^\prime) = \langle \phi(x, t) \phi(y, t^\prime) \rangle.
\end{equation}
The evolution equations for $F$ and $\rho$ are given in \cite{berges_introduction_2004}.
These equations involve memory integrals, which integrate over the time history of the evolution.
Comparing to Eq.~\ref{equ:twopoint} yields for the Fourier transform of $F$ in for
spacially homogeneous media configurations
\begin{equation}
    \label{eqn:Fttwo}
    F(t, t^{\prime}, x-y) = \frac{n_p+1/2}{\omega_p} \cos{ \big( \omega_p (t-t^{\prime}) \big) }.
\end{equation}
Note that since $F(t, t^{\prime}, x-y)$ is real only the real part of the two-point function contributes.
Evaluating $F(t, t^{\prime}, x-y)$ at equal times, allows to infer the particle number per mode.

\subsection{Interacting theory}\label{ssec:InteractTheory}
To obtain the particle number for the interacting theory one assumes that the 
form of the two point function remains the same as for the free theory, but with a different energy per mode
$e_p$. Yielding the following expression for the real part of the two point function 
\begin{equation}
    F(t, t^{\prime}, x-y) = \frac{n_p+1/2}{e_p} \cos{ \big( e_p (t-t^{\prime}) \big) }.
\end{equation}
The energy per mode can be extracted by
\begin{equation}
    \label{equ:energy}
    e_p = \Big( \frac{\partial_t \partial_{t^\prime} F(t,t^\prime,x-y)}{F(t,t^\prime,x-y)}\Big|_{t=t^\prime} \Big)^{1/2}.
\end{equation}
Therefore, only $\partial_t \partial_{t^\prime} F(t,t^\prime,x-y) $ and $F(t,t,x-y)$
are needed to obtain $n_p$ in the interacting theory.

\section{Numerical methods}\label{sec:Numerics}
\subsection{Leap frog integration}
The classical e.o.m., given in Eq.~\ref{eqn:eom}, are integrated using the leap frog method,
which is a symplectic one.
In the first step the $\phi$ field is advanced by a half time step (the subscript denotes the ith time step):
\begin{equation}
    \pi^a_{1} = \pi^a_{0} + \frac{dt}{2}  \dot{\pi}^a = 
   \pi^a_{0} - \frac{dt}{2}  \frac{\delta \mathcal{H}}{\delta \phi^a} (\phi^a_0).
\end{equation}
Subsequent evolution steps are performed as follows
\begin{align}
    \phi^a_{i+2} = \phi^a_{i} + dt \dot{\phi}^a_{i}  
    = \phi^a_{i} + dt  \frac{\delta \mathcal{H}}{\delta \pi^a} (\pi^a_{i+1}), \\
    \pi^a_{i+2} = \pi^a_{i} + dt \dot{\pi}^a  
    = \pi^a_{i} - dt  \frac{\delta \mathcal{H}}{\delta \phi^a} (\phi^a_{i+1})
\end{align}
In this notation the time index of $\phi$ ($\pi$) is always even (odd), emphasising that 
after a certain evolution, the two fields are actually not at the same time slice.
The symplectic integrator has the advantage of conserving the energy in the time evolution.
In Ref.~\cite{leapfrog} an analytical expression for the energy at each time step is derived for
the case of the simple harmonic oscillator. The amplitude in the energy oscillation
is shown to be proportional to the time step size squared. 
Note that the kick off step introduces an error, which is corrected after some integration steps.

\subsubsection{Implementation check}
A convenient way of checking the implementation is to draw random numbers for the $\phi$ and $\psi$ field:
\begin{equation}
    \phi \sim \mathcal{N}(\mu_{\phi}, \sigma_{\phi}) \quad \text{and} \quad \pi \sim \mathcal{N}(\mu_{\pi}, \sigma_{\pi}).
\end{equation}
The expectation value for the energy can then be calculated with the non-central moments of the normal distribution:
\begin{align}
    \langle x \rangle &= \mu, \\
    \langle x^2 \rangle &= \mu^2 + \sigma^2, \\
    \langle x^4 \rangle &= \mu^4 + 6\mu^2\sigma^2 + 3\sigma^4.
\end{align}
The expectation value for the energy is given by
\begin{align}
    \langle H \rangle &=  a^d \sum_{\text{sites}}
    \frac{1}{2} \langle \pi^a \pi^a \rangle
    + \frac{M^2}{2} \langle \phi^a \phi^a \rangle
    + \frac{\lambda}{4} \langle \phi^a \phi^a \phi^b \phi^b \rangle
    + \frac{1}{2} \langle \nabla \phi^a \nabla \phi^a \rangle \\
    &=  (aN_x)^d \Bigg(
        \frac{N}{2} ( \mu^2_{\pi} + \sigma^2_{\pi})
    + \frac{N M^2}{2} ( \mu^2_{\phi} + \sigma^2_{\phi})
    + \frac{\lambda}{4} \Big( N (4 \mu^2_{\phi} \sigma^2_{\phi}+2\sigma^4_{\phi}) 
    + N^2 (\mu^4_{\phi} + 2 \mu^2_{\phi} \sigma^2_{\phi}+\sigma^4_{\phi}) \Big) 
    + \frac{N d}{a^2} \sigma_{\phi}^2 \Bigg)
\end{align}
The expectation value of the derivative depends on the implementation of the derivative on the lattice.
For the forward derivative it is given by
\begin{align}
    \frac{1}{2} \langle \nabla \phi^a \nabla \phi^a \rangle &= 
    \frac{1}{2 a^2} \langle \big( \phi^a(x+a)- \phi^a(x) \big)^2 \rangle = \frac{N}{a^2} \sigma_{\phi}^2.
\end{align}
The interaction term requires careful evaluation:
\begin{align}
    \langle \phi^a \phi^a \phi^b \phi^b \rangle &= 
    \langle \phi^a \phi^a \phi^a \phi^a \rangle + 
    \langle \phi^a \phi^a \phi^b \phi^b \rangle_{a \neq b} = 
    N \langle \phi^4 \rangle + N(N-1) \langle \phi^2 \rangle^2.
\end{align}

\subsection{Fourier transformation}
In order to initialize the fields and evaluate the particle number per mode a Fourier transformation 
has to be performed. 
The lattice in coordinate space in one dimension is given by $x_i= i \times a$, where $a$ is the lattice spacing 
and $i\in (0,N-1)$. With periodic boundary conditions the plane waves are given by
\begin{equation}
    e^{ipx}, \quad p = n \frac{2 \pi}{N a}, \quad n \in (-N/2+1,.., N/2).
\end{equation}
The Fourier transform is defined as
\begin{align}
    f(x_n) = N^{-1/2} \sum_m \tilde{f}(p_m) e^{i n m\frac{2 \pi}{N a}}, \\
    \tilde{f}(p_m) = N^{-1/2} \sum_n f(x_n) e^{i n m\frac{2 \pi}{N a}}.
\end{align}
This can be extended for a d dimensional Fourier transformation.
The momenta are then given by $\vec{p} = \vec{n} \frac{2 \pi}{N a}$, where each component of n
is in $(-N/2+1,.., N/2)$. Therefore, the max value of $p$ is given by  $p_{max} = \sqrt{d} \frac{\pi}{a}$.
A point in Fourier space corresponds to a plane wave moving in direction of $p$. 
All waves with same value of $p$ lie on a circle in Fourier space (in the non discretized form).
Therefore, the Fourier space lattice points characterising a wave with certain momentum norm increase with
the momentum. 
For real fields $f(x)$ the Fourier transform has a symmetry $\tilde{f}(\vec{p})^*=\tilde{f}(-\vec{p})$,
reducing the number of independent components by two (in any dimension).
This implies, that the zero component and Nyquist frequencies (e.g $n=N/2$ in one dimension)
have no imaginary part.

\subsection{Initial conditions}
The two point function, in terms of the Fourier component is given by
\begin{align}
F(t,t^\prime, x-y) =
\langle \phi(x,t) \phi(y,t^\prime)\rangle = \int \frac{dk^3 }{(2\pi)^3}
\tilde{\phi}_p (t) \tilde{\phi}_{-p} (t^\prime) e^{-ip(x-y)}.
\end{align}
Thus the Fourier transform of the two point function is given by
\begin{align}
F(t,t^\prime, p) = \tilde{\phi}_p (t) \tilde{\phi}_{-p} (t^\prime) = 
\tilde{\phi}_p (t) \tilde{\phi}_{p}^* (t^\prime).
\end{align}
The last equality follows from $\phi(x) \in \mathcal{R}$.
At $t=0$, the initial conditions are chosen to be Gaussian fields without particles 
\begin{align}
    \tilde{\phi}_p (0) \tilde{\phi}_{-p} (0) = \frac{1}{2 \omega_p} \quad \text{with} \quad 
\omega_p = \sqrt{p_L^2+M^2}.
\end{align}
$p_L$ refers to the discretized momenta on the lattice.
The equation is to be understood in a probabilistic sense.
To obtain fluctuations in the Fourier transform $\phi_p$, which is a complex function
we could initialize real part and imaginary part by a Gaussian normal distribution.
This corresponds to a Rayleigh distribution for the amplitude and
a uniform distribution for the phase.
Since it requires to manually enforce the symmetry $\tilde{\phi}_{-p}=\tilde{\phi}_{p}^*$,
this is unconvenient.
An alternative way is the initialisation in coordinate space with a Gaussian distribution, which corresponds
also to Gaussian for real and imaginary part in Fourier space with inverse width. 
Imaginary and Real per mode part can then be altered by $\sqrt( \frac{1}{2 \omega_p})$, 
to obtain the desired fluctuations.
Appendix \ref{sec:init} explains this in more detail.
After these steps a vacuum expectation value (vev) is added to the first component.
This is totally general since if the vev is assigned to another direction, a orthonormal
transformation can bring it to the first component without changing the Lagrangian.
The momenta are initialized deterministically at value $0$ in coordinate space.
\subsection{Particle number}
To obtain the energy per mode quoted in Eq.~\ref{equ:energy}, the double
time derivative of $F$ is needed. It can be obtained by
\begin{align}
    \partial_t \partial_{t^{\prime}} F(t,\tilde{t}, p)|_{t=t^{\prime}} =
\partial_{t} \tilde{\phi}_p (t) \partial_{t^{\prime}} \tilde{\phi}_{-p} (t^{\prime})|_{t=t^{\prime}} =
\Big(\frac{ \tilde{\phi}_p (t)-\tilde{\phi}_p (t-dt)}{dt}\Big)
\Big(\frac{ \tilde{\phi}_p (t)-\tilde{\phi}_p (t-dt)}{dt}\Big)^*.
\end{align}
Since the initial conditions and the evolution is isotropic, it suffices to monitor $n_p$ for one
proxy of $p$. But to increase the reliability of the result, an average of all values to one momentum norm is calculated.
Note, that if the system were not isotropic $n_p$ would depend also on the direction of $p$, not only its magnitude. 

\section{Results}\label{sec:Results}

\subsection{Energy conservation}
Fig. \ref{fig:econs} shows oscillations in the energy for two different integration step sizes for an interacting field.
As the analytical result for the free field suggests \cite{leapfrog}, the oscillation amplitude increases with step size $dt$.
\begin{figure}[h]
     \centering
     \begin{subfigure}[b]{0.45\textwidth}
         \centering
         \includegraphics[width=1.\textwidth]{img/N20_d3_a1_dtm22_ON4_M1_L4_vev5i1_det_Energy.png}
         %\caption{$y=x$}
         %\label{fig:y equals x}
     \end{subfigure}
     %\hfill
     \begin{subfigure}[b]{0.45\textwidth}
         \centering
         \includegraphics[width=1.\textwidth]{img/N20_d3_a1_dtm32_ON4_M1_L4_vev5i1_det_Energy.png}
         %\caption{$y=3sinx$}
         %\label{fig:three sin x}
     \end{subfigure}
     \caption{Comparison of energy oscillation during leap frog integration for two integration step sizes:
         Left figure $dt=0.02$ and right figure $dt=0.002$. 
     Simulation parameters: $N_x=20$, dim$=3$, $a=1$, $M=1$, $N=4$, $\lambda=4$, Fields are deterministically initialized (without any fluctuations) with
     expectation values $\langle \phi^0 \rangle = 5$ and $\langle \phi^{a \neq 0} \rangle = 1$.}
     \label{fig:econs}
\end{figure}
\subsection{Field oscillations}
Fig.~\ref{fig:fosz} shows field oscillations for deterministically initialized fields.
On the left the result for a free theory is shown. This corresponds to a harmonic oscillator with frequency $\omega = M$, since
no fluctuations are present. Therefore, an oscillation period takes $2 \pi$ time units. Every component oscillates independently.
On the right the oscillations for the same configuration plus interaction is shown. The field components influence each other, 
resulting in a complicated oscillation pattern. The system corresponds to an anharmonic oscillator. The same solution is
obtained by performing the simulation on a bigger lattice ($N_x=50$), therefore finite size effects can be ruled out. 
No damping of the oscillations has been found for larger simulation times. 
Since no fluctuations are introduced in both cases, the variance is always zero.
\begin{figure}[h]
     \centering
     \begin{subfigure}[b]{0.45\textwidth}
         \centering
         \includegraphics[width=1.\textwidth]{img/N20_d3_a1_dtm32_ON4_M1_L0_vev5i1_det_Fields_raster.png}
         %\caption{$y=x$}
         %\label{fig:y equals x}
     \end{subfigure}
     %\hfill
     \begin{subfigure}[b]{0.45\textwidth}
         \centering
         \includegraphics[width=1.\textwidth]{img/N20_d3_a1_dtm32_ON4_M1_L4_vev5i1_det_Fields_raster.png}
         %\caption{$y=3sinx$}
         %\label{fig:three sin x}
     \end{subfigure}
     \caption{Comparison of field oscillations for the free and interacting $O(4)$ model.
     Shown are $\langle \phi \rangle$ and $var(\phi) = \langle \phi^2 \rangle - \langle \phi \rangle^2$ for each component. 
     Left Fig. $\lambda=0$ and right Fig. $\lambda=4$. 
     Simulation parameters: $N_x=20$, dim$=3$, $a=1$, $dt=0.002$, $N=4$, Fields are deterministically initialized (without any fluctuations) with
     expectation values $\langle \phi^0 \rangle = 5$ and $\langle \phi^{a \neq 0} \rangle = 1$.}
     \label{fig:fosz}
\end{figure}

Fig.~\ref{fig:foszfluct} shows an interacting field with initial fluctuations.
The left figure shows $\langle \phi \rangle$ and $var(\phi) = \langle \phi^2 \rangle - \langle \phi \rangle^2$ for each component, 
as before. The right figure shows the amplitude of $\langle \phi \rangle$ and also $var(\phi) = \langle \phi^2 \rangle - \langle \phi \rangle^2$ for each component. 
The amplitude is obtained by a Hilbert transformation.
The field expectation value of component one decreases with time and pushes the variances to higher values. 
But for longer simulation times the field expectation value of component one does not decrease any further (longest simulation
ran 400 time units).

\begin{figure}[h]
     \centering
     \begin{subfigure}[b]{0.45\textwidth}
         \centering
         \includegraphics[width=1.\textwidth]{img/N20_d3_a1_dtm32_ON4_M1_Lm1_vev10/N20_d3_a1_dtm32_ON4_M1_Lm1_vev10_Fields_raster.png}
         %\caption{$y=x$}
         %\label{fig:y equals x}
     \end{subfigure}
     %\hfill
     \begin{subfigure}[b]{0.45\textwidth}
         \centering
         \includegraphics[width=1.\textwidth]{img/N20_d3_a1_dtm32_ON4_M1_Lm1_vev10/N20_d3_a1_dtm32_ON4_M1_Lm1_vev10_FieldsAmplitude_raster.png}
         %\caption{$y=3sinx$}
         %\label{fig:three sin x}
     \end{subfigure}
     \caption{Field oscillations for an interacting field with initial fluctuations in the $O(4)$ model.
     The left figure shows $\langle \phi \rangle$ and $var(\phi) = \langle \phi^2 \rangle - \langle \phi \rangle^2$ for each component. 
     The right figure shows the amplitude of $\langle \phi \rangle$ and also $var(\phi) = \langle \phi^2 \rangle - \langle \phi \rangle^2$ for each component. 
     Simulation parameters: $N_x=20$, dim$=3$, $a=1$, $dt=0.002$, $N=4$, $\lambda=0.1$, $\langle \phi^0 \rangle = 10$ and $\langle \phi^{a \neq 0} \rangle = 0$.}
     \label{fig:foszfluct}
\end{figure}


\subsection{Initial fluctuations}
Fig. \ref{fig:startfluc} shows the initialized fluctuations of the $\phi$ field.
For each $p$ value, the corresponding fluctuation is distributed according to a Rayleigh distribution.
After initializing the fluctuations a vev for the zero field component is added.
\begin{figure}[h]
    \centering
    \includegraphics[width=0.6\textwidth]{img/N20_d3_a1_dtm32_ON4_M1_Lm1_vev10/N20_d3_a1_dtm32_ON4_M1_Lm1_vev10_Startflucutation_raster.png}
    \caption{Initialized fluctuations for the $O(4)$ model. The only relevant parameter is the Mass $M=1$.}
    \label{fig:startfluc}
\end{figure}
 
\subsection{Particle number}
Fig.~\ref{fig:occu} shows the occupation number per mode.
The occupation numbers grow with time, and furthermore moves towards the infrared.
The decaying field expectation value creates particles as required for the standard model of cosmology.
This is consistent with \cite{micha_turbulent_2004}. The only difference is the momenta scaling.
In Fig.~\ref{fig:occu} the max momenta is $\sqrt{3}\frac{\pi}{a}\approx 5.4$, whereas in 
\cite{micha_turbulent_2004} the x-axis is rescaled by the time depending amplitude of $ \langle \phi \rangle$.
For higher vevs at $t=0$ a substantially higher particle production has been found. 
\begin{figure}[h]
    \centering
    \includegraphics[width=0.6\textwidth]{img/N20_d3_a1_dtm32_ON4_M1_Lm1_vev10/N20_d3_a1_dtm32_ON4_M1_Lm1_vev10_Occupationnumber_raster_times.png}
    \caption{Occupation number per mode for the four field components. Shown are four time steps $t=0$, $13.2$, $26.6$ and $39.8$.
     Simulation parameters: $N_x=20$, dim$=3$, $a=1$, $dt=0.002$, $N=4$, $\lambda=0.1$, $\langle \phi^0 \rangle = 10$ and $\langle \phi^{a \neq 0} \rangle = 0$.}
    \label{fig:occu}
\end{figure}
 
\subsection{Fourier transform of two-point function}
Finally, Fig.~\ref{fig:F} shows the Fourier transform of the two-point function for some momentum values.
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.6\textwidth]{img/N20_d3_a1_dtm32_ON4_M1_Lm1_vev10/N20_d3_a1_dtm32_ON4_M1_Lm1_vev10_F_raster.png}
    \caption{Fourier transform of the two-point function.
     Simulation parameters: $N_x=20$, dim$=3$, $a=1$, $dt=0.002$, $N=4$, $\lambda=0.1$, $\langle \phi^0 \rangle = 10$ and $\langle \phi^{a \neq 0} \rangle = 0$.}
    \label{fig:F}
\end{figure}
 
\section{Conclusion}
\label{sec:Concl}
In this project some results of Ref.~\cite{micha_turbulent_2004} have been reproduced.
The obtained graphs are consistent with the ones given in the mentioned reference.
The here presented results are obtained by mainly one simulation. To investigate the 
robustness of the predictions further simulations should be run. Especially with bigger lattice sizes,
to rule out finite size effects. This would also allow to resolve peaks in the occupation number 
graphs. The reliability of the deployed algorithm for calculating the envelope of the one point function has not been tested
sufficiently. Furthermore, the plots should be equipped with error bars. 

\newpage
\appendix
\section{Initialisation}
\label{sec:init}
This appendix provides a mathematical treatment of the relation between random initialisation in coordinate space and
its consequences for Fourier space, and vice versa.
\subsection{Coordinate space initialisation}
Let the field value at each lattice point be drawn from a normal distribution: 
\begin{equation}
    \phi_n \sim \mathcal{N}(0,\sigma) =  \mathcal{N}_\sigma.
\end{equation}
The real part of the Fourier transform is given by
\begin{align}
    \tilde{\phi}_m = \sum_n \phi_n  e^{-i n m\frac{2 \pi}{N a}} \quad \rightarrow \quad 
    \Re(\tilde{\phi}_m) = \sum_n \phi_n  \cos \big( n m\frac{2 \pi}{N a} \big)
\end{align}
Since $\phi_n$ is normal distributed, $\Re(\tilde{\phi}_m)$ is also normal distributed since it is a sum of normal distributed values. 
The cosine is just a scale factor ( if $X \sim P_X(x) $ then $Y=aX \sim \frac{1}{a} P_X(y/a)$).
Therefore, the summands, abbreviated as $y_n$, are distributed according to
\begin{align}
P_{Y_n}(y_n) = \frac{1}{\cos ( n m\frac{2 \pi}{N a})} \mathcal{N}_{\sigma} \Big( \frac{y_n}{ \cos ( n m\frac{2 \pi}{N a}) } \Big)
=  \mathcal{N}_{\sigma; c(n,m)} 
\end{align}
Since the probability distribution of a sum of random numbers corresponds to a
convolution of their probability distributions,
$  \Re(\tilde{\phi}_m) $ is distributed according to 
\begin{align}
    P(\Re(\tilde{\phi}_m)) =   \mathcal{N}_{\sigma; c(0,m)} \otimes  \mathcal{N}_{\sigma; c(1,m)}  \otimes  \mathcal{N}_{\sigma; c(2,m)} ...
\end{align}
By bringing this expression to Fourier space the convolutions simplify to plain multiplications, resulting in
\begin{align}
    \mathcal{F}(P(\Re(\tilde{\phi}_m))) = \prod_n \mathcal{F}( \mathcal{N}_{\sigma; c(n,m)}) =
    \prod_n \mathcal{N}_{1/\sigma; c(n,m)} = \mathcal{N}_{\sqrt{2 N/\sigma^2 }}
\end{align}
In deriving these equalities the fact that the Fourier transform of a Gaussian with width $\sigma$ is
again a Gaussian with width $1/\sigma$. In the last step $\sum_n \cos( \frac{2 \pi n k}{N})^2 = N/2$ was used.
Performing the inverse Fourier transformation gives
\begin{align}
    P(\Re(\tilde{\phi}_m)) = \mathcal{N}_{\sigma \sqrt{N/2}}
\end{align}
Note that the right hand side does not depend on $k$.
The same result is to be obtained for the imaginary part.
If a symmetric normalisation of the Fourier transform is used, the $N$ dependence of the final result is absent.
\subsection{Fourier space initialisation}
The initial fluctuations are defined as
\begin{align}
\tilde{\phi}_p (0) \tilde{\phi}_{p}^* (0) = \frac{1}{2 \omega_p}
\end{align}
Since $\tilde{\phi}_p$ are complex numbers, this expression does no provide a recipe 
how to actually perform this initialisation, since it only gives a value for the norm of $\tilde{\phi}_p$.
Therefore the question arises, how is $|\tilde{\phi}_p|$ distributed if we require Gaussian fields.
As shown in the previous section, a Gaussian field in coordinate space corresponds to
a Gaussian field in the real and imaginary components of Fourier space.
Since $|\tilde{\phi}_p| = \sqrt{  \Re (\tilde{\phi}_p)^2 + \Im (\tilde{\phi}_p)^2 }$, the solution is given by
the $\chi$ distribution: If $X_i \sim \mathcal{N}(0,\sigma) $ for $i \in (1..j)$ then
$ Y = \sqrt{ \sum X_i} $ is $\chi$ distributed with $j$ degrees of freedom.
The $\chi$ distribution with 2 d.o.f., is also known as Rayleigh distribution:
\begin{align}
    P(x, \sigma) = \frac{x}{\sigma^2} \exp^{-\frac{x^2}{2 \sigma^2}}
\end{align}
then it follows that
\begin{align}
    \langle x^2 \rangle = \int_{0}^{\infty} x^2 P(x, \sigma) dx = 2 \sigma^2
\end{align}
Comparing to the initial fluctuations, tells us that we could initialize in
Fourier space by drawing the norm squared $|\tilde{\phi}_p|^2$ from the Rayleigh distribution with $\sigma=\sqrt{\omega_k}$
and by drawing the phase from a uniform distribution.
For completion it can be noted that the $\chi$ distribution with 3 d.o.f.,
widely known as Maxwell-Boltzmann distribution, describes
the probability distribution to find a particle with velocity $|\vec{v}|$ in a ideal gas in three dimensions,
where the velocity components per dimension are Gaussian distributed.
And furthermore that it is not to be confused with the $\chi^2$ distribution, which is widely used for hypothesis testing.

\bibliographystyle{unsrt}
\bibliography{bib/biblio.bib}
\end{document}
