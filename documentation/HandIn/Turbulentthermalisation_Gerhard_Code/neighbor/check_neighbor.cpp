// g++ check_neighbor.cpp neighbor.cpp -o run.check_neighbor && ./run.check_neighbor
#include "neighbor.h"
#include <cmath>
#include <iostream>

int main(int argc, char** argv)
{
    int64_t Nx=4;
    int dim=3;

    std::cout << "F ordering" << std::endl; 
    neighbor myneighbor_f(Nx,dim,"f");
    std::cout << "Position / which dimension / Up and Dn neigbors" << std::endl; 
    for(int64_t pos=0;pos<int(pow(Nx,dim));pos++){
        std::cout << "Pos: " << pos << "\t";
        for(int d=0;d<dim;d++){
            if (d!=0) std::cout << "\t";
            std::cout << "d=" << d << "\t ";
            std::cout << myneighbor_f.getUp(pos, d) << " " << myneighbor_f.getDn(pos, d) << std::endl;
        }
    }

    std::cout << "C ordering" << std::endl; 
    neighbor myneighbor_c(Nx,dim,"c");
    std::cout << "Position / which dimension / Up and Dn neigbors" << std::endl; 
    for(int64_t pos=0;pos<int(pow(Nx,dim));pos++){
        std::cout << "Pos: " << pos << "\t";
        for(int d=0;d<dim;d++){
            if (d!=0) std::cout << "\t";
            std::cout << "d=" << d << "\t ";
            std::cout << myneighbor_c.getUp(pos, d) << " " << myneighbor_c.getDn(pos, d) << std::endl;
        }
    }
}
