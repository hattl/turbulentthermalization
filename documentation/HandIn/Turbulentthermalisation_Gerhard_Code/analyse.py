import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pandas as pd
import os
import sys
from scipy.signal import hilbert # for the envelope

#
# Prelim
#
directory="/home/leonovo/LQFT_Exersices/Project_turbtherm/Results"
filename="N20_d3_a1_dtm32_ON4_M1_Lm15_vev10.txt"
colors = ["red","blue","green","yellow"]

#
# load data and infer nrcomponents and nrlevels
#
#  (1st line = header, 2nd line = momenta on lattice, 3-(3+comp) startflucutations in phi field per component
dftmp = pd.read_csv( os.path.join(directory, filename), sep=",", skiprows=[1]) #, header=0 
components = sum( [ x.startswith("OnePoint") for x in dftmp.columns.tolist()]) 
print("Number of components:" + str(components))
levels = sum( [ x.startswith("n_") and x.endswith("comp0") for x in dftmp.columns.tolist()]) 
print("Number of levels:" + str(levels))
df = pd.read_csv( os.path.join(directory, filename), sep=",", skiprows=[1]+[i for i in range(2, components+2)]) # skip line1 (k values) and lines components
momenta = np.loadtxt( os.path.join(directory, filename), skiprows=1, max_rows=1) #, header=0 
np.loadtxt( os.path.join(directory, filename), skiprows=1, max_rows=1) #, header=0 
startfluct = []
for i in range(components):
    startfluct.append( np.loadtxt( os.path.join(directory, filename), skiprows=2+i, max_rows=1)) 

############################# SELECT TIMES AND MOMENTA#########################################
#times = [min(df.time),df.time[len(df.time)/5+1],df.time[len(df.time)/2+1],max(df.time)]
times = [min(df.time),df.time[int((1*len(df.time))/3.)],df.time[int((2*len(df.time))/3.)],max(df.time)]
mom = [ int(1*len(momenta)/4), int(len(momenta)/2),int(3*len(momenta)/4)]
############################# PLOTS VS TIME ###################################################
#
# Energyplot
#
plotname ="Energy.png"
plt.plot( df.time, df.Energy, label='Energy')
plt.xlim( [0, max(df.time)])
plt.xlabel('time')
#plt.ylim( [0, max(df.Energy)])
plt.ylabel('Energy')
plt.legend(loc="upper right")
#plt.show()
plt.title("System energy")
plt.savefig( os.path.join( directory, filename.strip(".txt") + "_" + plotname))
plt.clf()

#
# Onepointfuncion and variance of field plot
#
plotname ="Fields_raster.png"
fig, ax = plt.subplots(nrows=2, ncols=int(components/2))
# infer y lim, st every subplot has the same
ymaxlist = []
yminlist = []
for i in range(components):
    ymaxlist.append(max(df["OnePoint_" + str(i)]))
    ymaxlist.append(max(df["Variance_" + str(i)]))
    yminlist.append(min(df["OnePoint_" + str(i)]))
    yminlist.append(min(df["Variance_" + str(i)]))
ymax = max(ymaxlist)
ymin = min(yminlist)

for i in range(components):
    plt.subplot(2,int(components/2), i+1)
    plt.plot( df.time, df["OnePoint_" + str(i)], color='black', label=r'$\phi$' + str(i))
    plt.plot( df.time, df["Variance_" + str(i)], color='red',  label=r'$var(\phi)$' + str(i))
    plt.xlim( [0, max(df.time)])
    plt.ylim( [ymin, ymax])
    plt.xlabel('time')
    plt.ylabel('Fields')
    plt.legend(loc="upper right")
#plt.yscale("log") //for oszillations not good
#plt.show()
fig.suptitle("One point function and variance")
plt.savefig( os.path.join( directory, filename.strip(".txt") + "_" + plotname))
plt.clf()

#
# Amplitude of one-point function
#
plotname ="FieldsAmplitude_raster.png"
fig, ax = plt.subplots(nrows=2, ncols=int(components/2))
# infer y lim, st every subplot has the same
ymaxlist = []
yminlist = []
for i in range(components):
    analytic_signal = hilbert(df["OnePoint_" + str(i)])
    amplitude_envelope = np.abs(analytic_signal)
    ymaxlist.append(max(amplitude_envelope))
    ymaxlist.append(max(df["Variance_" + str(i)]))
    yminlist.append(max(amplitude_envelope))
    yminlist.append(min(df["Variance_" + str(i)]))
ymax = max(ymaxlist)
ymin = min(yminlist)

for i in range(components):
    plt.subplot(2,int(components/2), i+1)
    analytic_signal = hilbert(df["OnePoint_" + str(i)])
    amplitude_envelope = np.abs(analytic_signal)
    plt.plot( df.time, amplitude_envelope ,      color='black', label=r'$\phi_A$' + str(i))
    plt.plot( df.time, df["Variance_" + str(i)], color='red' , label=r'$var(\phi)$' + str(i))
    plt.xlim( [0, max(df.time)])
    plt.ylim( [ymin, ymax])
    plt.xlabel('time')
    plt.ylabel('Fields')
    plt.legend(loc="upper right")
    plt.yscale("log")
fig.suptitle("One point function amplitude and variance")
plt.savefig( os.path.join( directory, filename.strip(".txt") + "_" + plotname))
plt.clf()

#
# F(t,t,p)
#
plotname ="F_raster.png"
fig, ax = plt.subplots(nrows=2, ncols=int(components/2))
# infer y lim, st every subplot has the same
ymaxlist = []
yminlist = []
for i in range(components):
    for m in mom:
        ymaxlist.append( max(df["F_lev" +str(m) + "_comp"+ str(i)]) )
        yminlist.append( min(df["F_lev" +str(m) + "_comp"+ str(i)]) )
ymax = max(ymaxlist)
ymin = min(yminlist)

for i in range(components):
    plt.subplot(2,int(components/2), i+1)
    for c,m in enumerate(mom):
        plt.plot( df.time, df["F_lev" +str(m) + "_comp"+ str(i)], color=colors[c], label='F(p=%.1f)'%momenta[m] + str(i))
    plt.xlim( [0, max(df.time)])
    plt.ylim( [ymin, ymax])
    plt.xlabel('time')
    plt.ylabel('F(t,t,p)')
    plt.legend(loc="upper right")
    #plt.yscale("log")
fig.suptitle("Fourie tranform of one point function")
plt.savefig( os.path.join( directory, filename.strip(".txt") + "_" + plotname))
plt.clf()

#
# enery per mode
#
plotname ="enerypermode_raster.png"
fig, ax = plt.subplots(nrows=2, ncols=int(components/2))
# infer y lim, st every subplot has the same
ymaxlist = []
yminlist = []
for i in range(components):
    for m in mom:
        ymaxlist.append( max(df["e_lev" +str(m) + "_comp"+ str(i)]))
        yminlist.append( min(df["e_lev" +str(m) + "_comp"+ str(i)]))
ymax = max(ymaxlist)
ymin = min(yminlist)

for i in range(components):
    plt.subplot(2,int(components/2), i+1)
    for c, m in enumerate(mom):
        plt.plot( df.time, df["e_lev" +str(m) + "_comp"+ str(i)], color=colors[c], label='e(p=%.1f)'%momenta[m] + str(i))
    plt.xlim( [0, max(df.time)])
    plt.ylim( [ymin, ymax])
    plt.xlabel('time')
    plt.ylabel(r'$e_p$')
    plt.legend(loc="upper right")
    #plt.yscale("log")
fig.suptitle("Energy per mode")
plt.savefig( os.path.join( directory, filename.strip(".txt") + "_" + plotname))
plt.clf()

############################# PLOTS VS MOMENTA ###################################################
#subaverage function
def subaverage(array, size=5):
    val = 0
    cnt = 0
    newarray = []
    for a in array:
        val += a
        cnt += 1
        if cnt==size:
            newarray.append(val)
            cnt = 0
            val = 0

    for i in range(len(newarray)):
        newarray[i] = newarray[i]/size
    return newarray
#
# Occupation number
#
# occupation nr
plotname ="Occupationnumber_raster.png"
fig, ax = plt.subplots(nrows=2, ncols=int(components/2))
# infer y lim, st every subplot has the same
ymaxlist = []
yminlist = []
for i in range(1,components): # only second component, since first starts of high 
    tmp = []
    for t in times:
        for lev in range(levels):
            tmp.append( df[df.time == t]["n_lev" + str(lev) + "_comp" + str(i)].values[0])
    ymaxlist.append( max(tmp))
    yminlist.append( min(tmp))
ymax = max(ymaxlist)
ymin = min(yminlist)

for i in range(components):
    plt.subplot(2,int(components/2), i+1)
    for c, t in enumerate(times):
        tmp = []
        for lev in range(levels):
            tmp.append( df[df.time == t]["n_lev" + str(lev) + "_comp" + str(i)].values[0])
        plt.plot( momenta, tmp, color=colors[c], label=r'$n_p(t=%.1f)$'%t + str(i))
    plt.xlim( [0, max(momenta)])
    plt.ylim( [ymin, ymax])
    #plt.ylim( [0, 1000])
    plt.xlabel('p')
    plt.ylabel(r"$n_p$")
    plt.legend(loc="upper right")
    #plt.yscale("log")
fig.suptitle("Occupation number per mode")
plt.savefig( os.path.join( directory, filename.strip(".txt") + "_" + plotname))
plt.clf()

# subaveraged
fig, ax = plt.subplots(nrows=2, ncols=int(components/2))
for i in range(components):
    plt.subplot(2,int(components/2), i+1)
    for c, t in enumerate(times):
        tmp = []
        for lev in range(levels):
            tmp.append( df[df.time == t]["n_lev" + str(lev) + "_comp" + str(i)].values[0])
        plt.plot( subaverage(momenta), subaverage(tmp), color=colors[c], label=r'$n_p(t=%.1f)$'%t + str(i))
    plt.xlim( [0, max(momenta)])
    plt.ylim( [ymin, ymax])
    #plt.ylim( [0, 1000])
    plt.xlabel('p')
    plt.ylabel(r"$n_p$")
    plt.legend(loc="upper right")
    #plt.yscale("log")
fig.suptitle("Occupation number per mode (averaged)")
plt.savefig( os.path.join( directory, filename.strip(".txt") + "_" + plotname.strip(".png") + "_subaverage.png"))
plt.clf()

#
# start fluct
#
plotname ="Startflucutation_raster.png"
fig, ax = plt.subplots(nrows=2, ncols=int(components/2))
# infer y lim, st every subplot has the same
ymaxlist = []
yminlist = []
for i in range(components):
    ymaxlist.append( max(startfluct[i]))
    yminlist.append( min(startfluct[i]))
ymax = max(ymaxlist)
ymin = min(yminlist)

for i in range(components):
    plt.subplot(2,int(components/2), i+1)
    plt.plot( momenta, startfluct[i], color='black', label=r'$\phi(p) \phi(p)^* (t=0) $'+ str(i))
    plt.xlim( [0, max(momenta)])
    plt.ylim( [ymin, ymax])
    plt.xlabel('p')
    plt.ylabel(r'$\phi(p) \phi(p)^* (t=0) $')
    plt.legend(loc="upper right")
    #plt.yscale("log")
fig.suptitle("Startflucutations")
plt.savefig( os.path.join( directory, filename.strip(".txt") + "_" + plotname))
plt.clf()

#
# distribution function
#
#plotname ="distributionfunction.png"
#tmpe = []
#tmpn = []
#for t in times:
#    for comp in range(components):
#    #for comp in range(1):
#        for lev in range(levels):
#            tmpe.append( df[df.time == t]["e_lev" + str(lev) + "_comp" + str(comp)].values[0])
#            tmpn.append( df[df.time == t]["e_lev" + str(lev) + "_comp" + str(comp)].values[0])
#        plt.plot( tmpe, [ np.log(1+1/n) for n in tmpn], color=colors[comp], label='t=' + str(t) + "_" + str(comp))
#        tmpe = []
#        tmpn = []
#    plt.xlim( [0, max(momenta)])
#    plt.xlabel('k')
#    plt.ylabel(r"$log(1+1/n)$")
#    plt.legend()
#    plt.yscale("log")
#    #plt.show()
#    plt.savefig( os.path.join( directory, filename.strip(".txt") + "_" + plotname.strip(".png") + "_time_" + str(t) + ".png"))
#    plt.clf()
