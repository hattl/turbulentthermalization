// g++ check_fftw_helper.cpp fftw_helper.cpp -o run.check_fftw_helper -lfftw3 -lm && ./run.check_fftw_helper
#include "fftw_helper.h"
#include <iostream>
#include <iomanip>


int main(int argc, char** argv)
{
    int64_t Nx=4;
    int d=2;

    std::cout << "D=" <<  d << std::endl; 
    std::cout << "Implementation of complex helper: " << std::endl; 
    fftw_helper helper(Nx, d);
    //fftw_helper helper(Nx, d, 5); // sets a max value for the normsquard to consider

    for (int lev=0; lev<helper.getnrlevels(); lev++){
        std::cout << "nr: " << lev << " value: "<< helper.getlevel(lev) << " degeneracy: " << helper.getdegeneracy(lev) << " fftw indices: ";
        for (int deg=0; deg<helper.getdegeneracy(lev); deg++){
                std::cout << helper.getindicesperlevel(lev,deg) << " ";
        }
        std::cout << std::endl; 
    }
}
