///////////////////////////////////////////////////////////////////////////////////////////////////////
//////                                                                                  ///////////////
//////                    TURBULENT THERMALISATION                                      ///////////////
//////                                                                                  ///////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

1. How to run
    1.1. g++ turbtherm.cpp fftwhelper/fftw_helper.cpp neighbor/neighbor.cpp -o run.turbtherm -lfftw3 -lm
    1.2. ./run.turbtherm 
    1.3. Creates one file with results
2. How to analyze
    2.1. change filename to the file you want to analyze in analyse.py
    2.2. python analyse.py
