\contentsline {section}{\numberline {1}Introduction}{1}%
\contentsline {section}{\numberline {2}Theory}{2}%
\contentsline {subsection}{\numberline {2.1}Linear sigma model}{2}%
\contentsline {subsection}{\numberline {2.2}Free theory}{2}%
\contentsline {subsection}{\numberline {2.3}Interacting theory}{3}%
\contentsline {section}{\numberline {3}Numerical methods}{3}%
\contentsline {subsection}{\numberline {3.1}Leap frog integration}{3}%
\contentsline {subsubsection}{\numberline {3.1.1}Implementation check}{4}%
\contentsline {subsection}{\numberline {3.2}Fourier transformation}{4}%
\contentsline {subsection}{\numberline {3.3}Initial conditions}{4}%
\contentsline {subsection}{\numberline {3.4}Particle number}{5}%
\contentsline {section}{\numberline {4}Results}{5}%
\contentsline {subsection}{\numberline {4.1}Energy conservation}{5}%
\contentsline {subsection}{\numberline {4.2}Field oscillations}{6}%
\contentsline {subsection}{\numberline {4.3}Initial fluctuations}{7}%
\contentsline {subsection}{\numberline {4.4}Particle number}{7}%
\contentsline {subsection}{\numberline {4.5}Fourier transform of two-point function}{7}%
\contentsline {section}{\numberline {5}Conclusion}{7}%
\contentsline {section}{\numberline {A}Initialisation}{8}%
\contentsline {subsection}{\numberline {A.1}Coordinate space initialisation}{8}%
\contentsline {subsection}{\numberline {A.2}Fourier space initialisation}{9}%
