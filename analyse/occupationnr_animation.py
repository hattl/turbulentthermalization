import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
import matplotlib.animation as animation

#
# Prelim
#
directory="/home/leonovo/LQFT_Exersices/Project_turbtherm/Results"
filename="N20_d3_a1_dtm32_ON4_M1_L2_vev5.txt"
colors = ["red","blue","green","yellow"]

#
# load data and infer nrcomponents and nrlevels
#
#  (1st line = header, 2nd line = momenta on lattice, 3-(3+comp) startflucutations in phi field per component
dftmp = pd.read_csv( os.path.join(directory, filename), sep=",", skiprows=[1]) #, header=0 
components = sum( [ x.startswith("OnePoint") for x in dftmp.columns.tolist()]) 
print("Number of components:" + str(components))
levels = sum( [ x.startswith("n_") and x.endswith("comp0") for x in dftmp.columns.tolist()]) 
print("Number of levels:" + str(levels))
df = pd.read_csv( os.path.join(directory, filename), sep=",", skiprows=[1]+[i for i in range(2, components+2)]) # skip line1 (k values) and lines components
momenta = np.loadtxt( os.path.join(directory, filename), skiprows=1, max_rows=1) #, header=0 
np.loadtxt( os.path.join(directory, filename), skiprows=1, max_rows=1) #, header=0 
startfluct = []
for i in range(components):
    startfluct.append( np.loadtxt( os.path.join(directory, filename), skiprows=2+i, max_rows=1)) 

#
# occupation nr animation
#
fig, ax = plt.subplots()
time_text = ax.text(0.80, 0.95-i*0.05,  '', transform=ax.transAxes)
xdata, ydata = [], []
l, = plt.plot([], [], 'ro')

# Set up formatting for the movie files
Writer = animation.writers['ffmpeg']
writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)

def init():
    ax.set_xlim(0,  max(momenta))
    ax.set_ylim( -.5, 100)
    time_text.set_text('')
    return l,

def update(num, df, line):
    comp = 0 # only 0th component for now
    tmp=[]
    t = df.time[num]
    for lev in range(levels):
        tmp.append( df[df.time == t]["n_lev" + str(lev) + "_comp" + str(comp)].values[0])
    xdata = momenta 
    ydata = tmp
    #xdata.append(frame)
    #ydata.append(np.sin(frame))
    line.set_data(xdata, ydata)
    time_text.set_text('t='+str(t))
    return line, time_text

# interval=time in ms, 
ani = animation.FuncAnimation(fig, update, len(df.time) , fargs=(df, l),
        interval=1000, repeat=False, init_func=init, blit=True)

plt.legend(loc="upper left")
#ani.save('occupationnr.mp4', writer=writer)
plt.show()

