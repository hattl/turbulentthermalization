// g++ turbtherm.cpp fftwhelper/fftw_helper.cpp neighbor/neighbor.cpp -o run.turbtherm -lfftw3 -lm && ./run.turbtherm 
// Note: FFT normalisation is based on FFTW (no normalisation). When calculatin measurements the required normalisation is hardcoded - symmetric normalisation is chosen.
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <random>
#include <fftw3.h>

#include "fftwhelper/fftw_helper.h"
#include "neighbor/neighbor.h"

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#define REAL 0
#define IMAG 1

//===================================================//
// define Model
//===================================================//
double Hamilton(fftw_complex ** phi, fftw_complex ** pi, neighbor &nn, int64_t Nx, int sdim, int ONgroup, double dx, double Mass, double Lambda){
    double Ham=0;
    double dx2=dx*dx;
    double phisqr;
	for(int pos=0;pos<int(pow(Nx,sdim));pos++){
        double phisqr=0;
	    for(int comp=0;comp<ONgroup;comp++){
		    Ham += 0.5*(pi[comp][pos][REAL]*pi[comp][pos][REAL]); // momentum term
		    phisqr +=  phi[comp][pos][REAL]*phi[comp][pos][REAL]; //prep for potential term, it is nonlinear in components
        }
		Ham += (Mass*Mass/2.)*phisqr+(Lambda/(4.))*phisqr*phisqr; // Potential term
	    for(int d=0;d<sdim;d++){
	        for(int comp=0;comp<ONgroup;comp++){
                Ham += (0.5/dx2)*(phi[comp][nn.getUp(pos,d)][REAL]-phi[comp][pos][REAL])*(phi[comp][nn.getUp(pos,d)][REAL]-phi[comp][pos][REAL]); // derivative phi term, forward
            }
        }
    }
	return Ham*pow(dx,sdim);
}

void dHdphi(double * update, fftw_complex ** phi, neighbor &nn, int64_t pos, int64_t Nx, int sdim, int ONgroup, double dx, double Mass, double Lambda){
    double phisqr=0;
    double dx2=dx*dx;
	for(int comp=0;comp<ONgroup;comp++){
	    phisqr += phi[comp][pos][REAL]*phi[comp][pos][REAL]; //prep for potential term, it is nonlinear in components
    }
    for(int comp=0;comp<ONgroup;comp++){
        update[comp] = (Mass*Mass)*phi[comp][pos][REAL] + Lambda*phi[comp][pos][REAL]*phisqr; // Potential term
	    for(int d=0;d<sdim;d++){
            update[comp] += -1./dx2 * (phi[comp][nn.getUp(pos,d)][REAL]-2*phi[comp][pos][REAL]+phi[comp][nn.getDn(pos,d)][REAL]); // derivative term
   }
 }
}

//===================================================//
// define initialization parameters
//===================================================//
struct Parameters {
  // lattice parameter
  int64_t Nx;
  double dx;
  int sdim;

  // Field properties
  int ONgroup;
  double Mass;
  double Lambda;

  // integration stepsize
  double dt;
};

class Lattice
{
  public:
    Lattice(Parameters InitParms);
    ~Lattice(); // Destructor

    // init fields
    void initfields(double sigma, double vev); // main init method
    void initphi_normal( double sigma); 
    void ftphi_timesoneoveromegak(double sigma); // sigma is needed for omegak
    void phi_addvev(double vev); // to 0th component
    void pi_zero();

    void init_allnormal(double muphi, double sigmaphi, double mupi, double sigmapi); //testing purpose
    void init_alldet(double phizeroth, double phiother); //testing purpose

    // Evolution
    void initEvolution(); // half pi step forward
    void compEvolution(); // half pi step backward
    void EvolveLattice(); // phi and pi full step forward

    // outfile
    void CreateFile(std::string filename);
    void WriteOutput(double t);
    void WriteInitialFluct();

    // Measurement
    void copyftphi_to_ftphi_oldtimeslice(); // Return one point function
    void calcEnergypermode(); //calculate Energy
    double getEnergypermode(int64_t level, int comp);
    double getF(int64_t level, int comp); //correctly normalized!
    double getOnePoint(int comp); // Return one point function
    double getOnePointsquared(int comp);
    double getVariance(int comp);
    double getOccupationnr(int64_t level, int comp); // Return one point function
    double getEnergy(); // Return energy at given configuration
    double getEnergyatequaltime(); // Return energy at equal time, does not change configuration
    
    // helpers
    void fft_forward_pi();
    void fft_forward_phi();
    void fft_backward_pi();
    void fft_backward_phi();
    void Printconfig();
    int64_t ReturnLatticeSize();
    double  ReturnLatticeSpacing();
    int ReturnLatticeDimension();

  private:
    // lattice parameter
    int64_t Nx;
    double dx;
    int sdim;
    neighbor nn;

    // field properties
    int ONgroup;
    double Mass;
    double Lambda;

    // fields
    fftw_complex** phi;
    fftw_complex** ftphi;
    fftw_complex** ftphi_oldslice;
    fftw_complex** pi; //not needed
    fftw_complex** ftpi; //not needed
    // tmp storage for update
    double* update;

    // integration stepsize
    double dt;

    // storage for F(t,t,p)
    double** F;
    double** energy;

    // fftw plans
    int* n; // nr of points per dimension, eg Nx, Nx, Nx in 3D
    fftw_plan* plan_phi_f;
    fftw_plan* plan_phi_b;
    fftw_plan* plan_pi_f; //not needed
    fftw_plan* plan_pi_b; //not needed
    fftw_helper fftwhelper; //declare member

    // fstream
    std::ofstream outfile;
};

//
// Init fields
//
void Lattice::initfields(double sigma, double vev)
{
    initphi_normal(sigma); //normaldist in coordinatespace and performs ft
    ftphi_timesoneoveromegak(sigma); //alter configuration in ft space
    phi_addvev(vev); // add vev in coordinate space for first component
    pi_zero(); // init pi to 0
} 

void Lattice::initphi_normal(double sigma)
{
    int64_t seed=3;
    std::mt19937 genrnd(seed);
    std::normal_distribution<double> Gauss( 0., sigma);
    for(int64_t pos=0;pos<int(pow(Nx,sdim));pos++) { 
	    for(int comp=0;comp<ONgroup;comp++) {
            phi[comp][pos][REAL]=Gauss(genrnd);
            phi[comp][pos][IMAG]=0.;
        }
    }
    fft_forward_phi(); //now we have white noise in ft spectrum
    double normalisation = sqrt(pow(Nx,sdim)); //perform normalisation in ftphi
	for(int comp=0;comp<ONgroup;comp++) {
        for(int64_t pos=0;pos<int(pow(Nx,sdim));pos++) { 
            ftphi[comp][pos][REAL]/=normalisation;
            ftphi[comp][pos][IMAG]/=normalisation;
        }
    }
}

void Lattice::ftphi_timesoneoveromegak(double sigma)
{
    double ksquared, omegak;
    int idx;
	for(int comp=0;comp<ONgroup;comp++) {
        for(int64_t lev=0;lev<fftwhelper.getnrlevels();lev++) {
            ksquared = fftwhelper.getlevel(lev)*(2*M_PI)/(dx*Nx)*(2*M_PI)/(dx*Nx); //fftwhelper returns nsquared
            omegak = sqrt(ksquared+Mass*Mass); 
            for(int deg=0;deg<fftwhelper.getdegeneracy(lev);deg++) { //every degeneracy!
                idx = fftwhelper.getindicesperlevel(lev, deg);
                ftphi[comp][idx][REAL] *= 1./sqrt(2.*omegak);
                ftphi[comp][idx][IMAG] *= 1./sqrt(2.*omegak);
            }
        }
    }
    fft_backward_phi(); //now we have quantum fluctuations in phi and ftphi
    double normalisation = sqrt(pow(Nx,sdim)); //perform normalisation in phi
	for(int comp=0;comp<ONgroup;comp++) {
        for(int64_t pos=0;pos<int(pow(Nx,sdim));pos++) { 
            phi[comp][pos][REAL]/=normalisation;
            phi[comp][pos][IMAG]=0.;
        }
    }
}

void Lattice::phi_addvev(double vev)
{
    for(int64_t pos=0;pos<int(pow(Nx,sdim));pos++) { 
        phi[0][pos][REAL]+=vev;
        phi[0][pos][IMAG]=0.;
    }
}

void Lattice::pi_zero()
{
	for(int comp=0;comp<ONgroup;comp++) {
        for(int64_t pos=0;pos<int(pow(Nx,sdim));pos++) { 
            pi[0][pos][REAL]=0.;
            pi[0][pos][IMAG]=0.;
        }
    }
}

void Lattice::init_allnormal(double muphi, double sigmaphi, double mupi, double sigmapi)
{
    int64_t seed=3;
    std::mt19937 genrnd(seed);
    std::normal_distribution<double> normal_phi( muphi, sigmaphi);
    std::normal_distribution<double> normal_pi( mupi, sigmapi);
    for(int64_t pos=0;pos<int(pow(Nx,sdim));pos++) { 
	    for(int comp=0;comp<ONgroup;comp++) {
            phi[comp][pos][REAL]=normal_phi(genrnd);
            phi[comp][pos][IMAG]=0.;
            pi[comp][pos][REAL]=normal_pi(genrnd);
            pi[comp][pos][IMAG]=0.;
        }
    }
    double Hexpected=0;
    Hexpected += ONgroup*std::pow(Nx,sdim)*0.5*(mupi*mupi+sigmapi*sigmapi); //pi term
    Hexpected += std::pow(Nx,sdim)*Lambda/(4.)*
        (ONgroup*(4*muphi*muphi*sigmaphi*sigmaphi+2*sigmaphi*sigmaphi*sigmaphi*sigmaphi)+
         (ONgroup*ONgroup*(muphi*muphi*muphi*muphi+2*muphi*muphi*sigmaphi*sigmaphi+sigmaphi*sigmaphi*sigmaphi*sigmaphi))); //interaction term
    Hexpected += ONgroup*std::pow(Nx,sdim)*0.5*Mass*Mass*(muphi*muphi+sigmaphi*sigmaphi); //mass term
    Hexpected += ONgroup*std::pow(Nx,sdim)*1./(2*dx*dx)*sdim*2*(sigmaphi*sigmaphi); //derivative term
    Hexpected *= std::pow(dx,sdim);
    std::cout << "Expected energy: " << Hexpected << std::endl;
}

void Lattice::init_alldet(double phizeroth, double phiother)
{
    for(int64_t pos=0;pos<int(pow(Nx,sdim));pos++) { 
        phi[0][pos][REAL]=phizeroth;
        phi[0][pos][IMAG]=0.;
        pi[0][pos][REAL]=0.;
        pi[0][pos][IMAG]=0.;
	    for(int comp=1;comp<ONgroup;comp++) {
            phi[comp][pos][REAL]=phiother;
            phi[comp][pos][IMAG]=0.;
            pi[comp][pos][REAL]=0.;
            pi[comp][pos][IMAG]=0.;
        }
    }
 
}    
//
// Evolution
//
void Lattice::initEvolution(){
  for(int64_t pos=0;pos<int(pow(Nx,sdim));pos++) { 
    dHdphi(update, phi, nn, pos, Nx, sdim, ONgroup, dx, Mass, Lambda);  //get update for all comp at pos
	for(int comp=0;comp<ONgroup;comp++) {pi[comp][pos][REAL] =  pi[comp][pos][REAL] - 0.5*dt*update[comp];}
  }
}

void Lattice::compEvolution(){
  for(int64_t pos=0;pos<int(pow(Nx,sdim));pos++) { 
    dHdphi(update, phi, nn, pos, Nx, sdim, ONgroup, dx, Mass, Lambda);  //get update for all comp at pos
	for(int comp=0;comp<ONgroup;comp++) {pi[comp][pos][REAL] =  pi[comp][pos][REAL] + 0.5*dt*update[comp];}
  }
}

void Lattice::EvolveLattice(){
  // Phi
  for(int64_t pos=0;pos<int(pow(Nx,sdim));pos++) { 
	  for(int comp=0;comp<ONgroup;comp++) {phi[comp][pos][REAL] =  phi[comp][pos][REAL] + dt*pi[comp][pos][REAL];}

  }
  // Pi
  for(int64_t pos=0;pos<int(pow(Nx,sdim));pos++) { 
    dHdphi(update, phi, nn, pos, Nx, sdim, ONgroup, dx, Mass, Lambda); //get update for all comp at pos
	for(int comp=0;comp<ONgroup;comp++) {
        pi[comp][pos][REAL] =  pi[comp][pos][REAL] - dt*update[comp];
    }
  }
}

//
// Write methods
//
//void Lattice::CreateFile(const char* filename) {
void Lattice::CreateFile(std::string filename) {
    outfile.open(filename);
    // header 1
    outfile <<  "time" 
            << "," << "Energy";
	        for(int comp=0;comp<ONgroup;comp++) {
    outfile << "," << "OnePoint_" << comp;
            }
	        for(int comp=0;comp<ONgroup;comp++) {
    outfile << "," << "Variance_" << comp;
            }
	        for(int comp=0;comp<ONgroup;comp++) {
                for(int lev=0;lev<fftwhelper.getnrlevels();lev++) { 
    outfile << "," << "n_lev" << lev << "_comp" << comp;
                }
            }
	        for(int comp=0;comp<ONgroup;comp++) {
                for(int lev=0;lev<fftwhelper.getnrlevels();lev++) { 
    outfile << "," << "F_lev" << lev << "_comp" << comp;
                }
            }
	        for(int comp=0;comp<ONgroup;comp++) {
                for(int lev=0;lev<fftwhelper.getnrlevels();lev++) { 
    outfile << "," << "e_lev" << lev << "_comp" << comp;
                }
            }
    outfile << std::endl;
    // header 2 - specifies the momenta on the lattice
    double kzero = (2*M_PI)/(Nx*dx);
    for(int lev=0;lev<fftwhelper.getnrlevels();lev++) { 
        outfile << sqrt(fftwhelper.getlevel(lev))*kzero << " ";
    }
    outfile << std::endl;
}

void Lattice::WriteInitialFluct(){
    // header 3 - writes initial fluctuation to file, per component one line
    // Note: after initializing, all fields are properly normalized
    double ftphi2 = 0; 
	for(int comp=0;comp<ONgroup;comp++) {
        for(int lev=0;lev<fftwhelper.getnrlevels();lev++){
            for(int deg=0;deg<fftwhelper.getdegeneracy(lev);deg++) { //calculate mean of ftphi2 with respect to degeneracy
                ftphi2 += ftphi[comp][fftwhelper.getindicesperlevel(lev, deg)][REAL]*ftphi[comp][fftwhelper.getindicesperlevel(lev, deg)][REAL];
                ftphi2 += ftphi[comp][fftwhelper.getindicesperlevel(lev, deg)][IMAG]*ftphi[comp][fftwhelper.getindicesperlevel(lev, deg)][IMAG];}
            outfile << ftphi2/fftwhelper.getdegeneracy(lev)<< " ";
            ftphi2=0;
        }
        outfile << std::endl;
    }
}


void Lattice::WriteOutput(double t){
    outfile <<  t 
            << "," << getEnergy();
	        for(int comp=0;comp<ONgroup;comp++) {
    outfile << "," << Lattice::getOnePoint(comp);
            }
	        for(int comp=0;comp<ONgroup;comp++) {
    outfile << "," << Lattice::getVariance(comp);
            }
	        for(int comp=0;comp<ONgroup;comp++) {
                for(int lev=0;lev<fftwhelper.getnrlevels();lev++) {
    outfile << "," << getOccupationnr(lev, comp);
                }
            }
	        for(int comp=0;comp<ONgroup;comp++) {
                for(int lev=0;lev<fftwhelper.getnrlevels();lev++) {
    outfile << "," << getF(lev, comp);
                }
            }
	        for(int comp=0;comp<ONgroup;comp++) {
                for(int lev=0;lev<fftwhelper.getnrlevels();lev++) {
    outfile << "," << getEnergypermode(lev, comp);
                }
            }
    outfile << std::endl;
}


//
// Measurement
//
void Lattice::copyftphi_to_ftphi_oldtimeslice(){
    for(int comp=0;comp<ONgroup;comp++){ 
        for(int64_t pos=0;pos<int(pow(Nx,sdim));pos++) {//pos is also fine for copying, no need for lev, deg 
            ftphi_oldslice[comp][pos][REAL]=ftphi[comp][pos][REAL];
            ftphi_oldslice[comp][pos][IMAG]=ftphi[comp][pos][IMAG];
        }
    }
}

void Lattice::calcEnergypermode(){
    double ftphi2=0;
    double dtdtftphi2=0;
    int64_t idx=0;
    // Note: number of degenerate states increases with k, since the problem is isotropic one direction suffices, but to increase statistics i average over degeneracies
	for(int comp=0;comp<ONgroup;comp++) {
        for(int64_t lev=0;lev<fftwhelper.getnrlevels();lev++) {
            for(int deg=0;deg<fftwhelper.getdegeneracy(lev);deg++) {
                idx =  fftwhelper.getindicesperlevel(lev, deg);
                ftphi2 += ftphi[comp][idx][REAL]*ftphi[comp][idx][REAL];
                ftphi2 += ftphi[comp][idx][IMAG]*ftphi[comp][idx][IMAG];
                dtdtftphi2 += std::pow((ftphi[comp][idx][REAL] - ftphi_oldslice[comp][idx][REAL]),2) + std::pow((ftphi[comp][idx][IMAG] - ftphi_oldslice[comp][idx][IMAG]),2);
            }
            ftphi2 /= fftwhelper.getdegeneracy(lev); //average
            dtdtftphi2 /= fftwhelper.getdegeneracy(lev); //average
            dtdtftphi2 /= dt*dt; //dt spacing of derivative
            F[comp][lev]=ftphi2/int(pow(Nx,sdim)); //store for np calculation, with proper normalization (recall 2 times normalisation, since two fields)
            energy[comp][lev] = sqrt(dtdtftphi2/ftphi2);
            dtdtftphi2 = 0;
            ftphi2 = 0;
        }
    }
}

double Lattice::getEnergypermode(int64_t level, int comp){ return energy[comp][level];}
double Lattice::getF(int64_t level, int comp){ return F[comp][level];}
double Lattice::getOccupationnr(int64_t level, int comp){ return energy[comp][level]*F[comp][level]-0.5;}

double Lattice::getOnePoint(int comp){
    double onepoint=0;
    for(int64_t pos=0;pos<int(pow(Nx,sdim));pos++) { 
        onepoint+=phi[comp][pos][REAL];
    }
    return onepoint/std::pow(Nx,sdim);
}

double Lattice::getOnePointsquared(int comp) {
    double onepoint=0;
    for(int64_t pos=0;pos<int(pow(Nx,sdim));pos++) { 
        onepoint+=(phi[comp][pos][REAL]*phi[comp][pos][REAL]);
    }
    return onepoint/std::pow(Nx,sdim);
}

double Lattice::getVariance(int comp) {return getOnePointsquared(comp)-pow(getOnePoint(comp),2);}

double Lattice::getEnergy() {return Hamilton(phi, pi, nn, Nx, sdim, ONgroup, dx, Mass, Lambda);}
double Lattice::getEnergyatequaltime()
{
  // complete half pi step
  compEvolution();
  // Measure
  double Energy = Hamilton(phi, pi, nn, Nx, sdim, ONgroup, dx, Mass, Lambda);
  // start pi half step
  initEvolution();
  
  return Energy;
}

//
// Helpers
//
void Lattice::fft_forward_phi(){ for(int comp=0;comp<ONgroup;comp++) fftw_execute(plan_phi_f[comp]);}
void Lattice::fft_forward_pi(){ for(int comp=0;comp<ONgroup;comp++) fftw_execute(plan_pi_f[comp]);}
void Lattice::fft_backward_phi(){ for(int comp=0;comp<ONgroup;comp++) fftw_execute(plan_phi_b[comp]);}
void Lattice::fft_backward_pi(){ for(int comp=0;comp<ONgroup;comp++) fftw_execute(plan_pi_b[comp]);}

void Lattice::Printconfig()
{
  std::cout << "*******CONFIG**START*********" << std::endl;
  for(int64_t pos=0;pos<int(pow(Nx,sdim));pos++) { 
    std::cout << "Pos: " << pos << ": ";
    std::cout << "Phi: ";
	std::cout << "[";
	for(int comp=0;comp<ONgroup;comp++) { std::cout << phi[comp][pos][REAL] << "/" << phi[comp][pos][IMAG] << ",";}
	std::cout << "] ";
    std::cout << "Pi: ";
	std::cout << "[";
	for(int comp=0;comp<ONgroup;comp++) { std::cout << pi[comp][pos][REAL] << "/" << pi[comp][pos][IMAG] << ",";}
	std::cout << "]";
    std::cout << std::endl;
  }
  std::cout << "*******CONFIG**END*********" << std::endl;
}
int64_t Lattice::ReturnLatticeSize() {return Nx;}
double Lattice::ReturnLatticeSpacing() {return dx;}
int Lattice::ReturnLatticeDimension() {return sdim;}

//
// Constructor
//
Lattice::Lattice(Parameters InitParms): 
    fftwhelper(InitParms.Nx, InitParms.sdim),
    nn(InitParms.Nx, InitParms.sdim, "c")// initializer list
{
  // lattice parameter
  this->Nx=InitParms.Nx;
  this->dx=InitParms.dx;
  this->sdim=InitParms.sdim;
  // field properties
  this->ONgroup=InitParms.ONgroup;
  this->Mass=InitParms.Mass;
  this->Lambda=InitParms.Lambda;
  // integration stepsize
  this->dt=InitParms.dt;

  // fields
  // allocate
  phi = new fftw_complex*[ONgroup];
  ftphi = new fftw_complex*[ONgroup];
  ftphi_oldslice = new fftw_complex*[ONgroup];
  pi = new fftw_complex*[ONgroup];
  ftpi = new fftw_complex*[ONgroup];
  for(int64_t comp=0;comp<ONgroup;comp++) { 
    phi[comp] = new fftw_complex[int(pow(Nx,sdim))];
    ftphi[comp] = new fftw_complex[int(pow(Nx,sdim))];
    ftphi_oldslice[comp] = new fftw_complex[int(pow(Nx,sdim))];
    pi[comp] = new fftw_complex[int(pow(Nx,sdim))];
    ftpi[comp] = new fftw_complex[int(pow(Nx,sdim))];
  }

  // init update storage
  update = new double[ONgroup];

  // init F and energy storage
  F = new double*[ONgroup];
  energy = new double*[ONgroup];
  for (int comp=0; comp<ONgroup;++comp){
      F[comp]=new double[fftwhelper.getnrlevels()];
      energy[comp]=new double[fftwhelper.getnrlevels()];
  }

  // init fftw plans
  plan_phi_f = new fftw_plan[ONgroup];
  plan_phi_b = new fftw_plan[ONgroup];
  plan_pi_f = new fftw_plan[ONgroup];
  plan_pi_b = new fftw_plan[ONgroup];
  int n[sdim]; // nr of points per dimension
  for (int d = 0; d < sdim; ++d) {
      n[d]=Nx;
  }

  for(int comp=0; comp<ONgroup;comp++){
    plan_phi_f[comp] = fftw_plan_dft( sdim, n, phi[comp], ftphi[comp], FFTW_FORWARD, FFTW_ESTIMATE);
    plan_phi_b[comp] = fftw_plan_dft( sdim, n, ftphi[comp], phi[comp], FFTW_BACKWARD, FFTW_ESTIMATE);
    plan_pi_f[comp] = fftw_plan_dft( sdim, n, pi[comp], ftpi[comp], FFTW_FORWARD, FFTW_ESTIMATE);
    plan_pi_b[comp] = fftw_plan_dft( sdim, n, ftpi[comp], pi[comp], FFTW_BACKWARD, FFTW_ESTIMATE);
  }
}

Lattice::~Lattice() {
    for (int comp=0; comp<ONgroup;++comp){
        fftw_destroy_plan(plan_phi_f[comp]);
        fftw_destroy_plan(plan_phi_b[comp]);
        fftw_destroy_plan(plan_pi_f[comp]);
        fftw_destroy_plan(plan_pi_b[comp]);
    }

    if (outfile.is_open()) outfile.close();
}

//
// Main
//

int main(int argc, char** argv)
{

    //===================================================//
    // Initialize the lattice 
    //===================================================//
    Parameters MyLatticeParms;
    // lattice parameter
    MyLatticeParms.Nx=20;
    MyLatticeParms.sdim=3;
    MyLatticeParms.dx=1.;
    // Field properties
    MyLatticeParms.ONgroup=4;
    MyLatticeParms.Mass=1;
    MyLatticeParms.Lambda=0.5;
    // integration stepsize
    MyLatticeParms.dt=0.002; //0.002

    //===================================================//
    // Simulation parameters
    //===================================================//
    int64_t Nsteps=2000; //20000

    double sigma = 1.0;
    double vev = 10.;
    //===================================================//
    // Paths
    //===================================================//
    // initialize datapath from paths.txt
    std::ifstream pathsFile("../paths.txt");
    std::string datapath;
    while(!pathsFile.eof()){
        getline(pathsFile,datapath);
        if (!datapath.find("data#")){
            datapath.erase(0,5);
            break;
        }
    }
    // sting of parameters
    std::string parameterstring =   "Nx" +      std::to_string(MyLatticeParms.Nx)
                                    + "_sdim" + std::to_string(MyLatticeParms.sdim) 
                                    + "_dt"   + std::to_string((int) (MyLatticeParms.dt*1000)) 
                                    + "_ON"   + std::to_string(MyLatticeParms.ONgroup)
                                    + "_L"    + std::to_string((int) (MyLatticeParms.Lambda*100))
                                    + "_M"    + std::to_string((int) (MyLatticeParms.Mass*100))
                                    + "_vev"  + std::to_string( (int) vev);

    // create folder system for string of parameters and for specific run
    boost::filesystem::create_directory( datapath );

    //
    // Simulation
    //
    Lattice turbtherm(MyLatticeParms);
    turbtherm.CreateFile( datapath + "/" + parameterstring + ".txt" );

    // 
    // init 
    //
    std::cout << "Inizializing fields... " << std::endl;
    turbtherm.initfields(sigma, vev); // main init method
    turbtherm.WriteInitialFluct();
    //turbtherm.Printconfig();
//////// 1. TEST: draw phi and pi from normal distribution and compare with anlythical result for Energy  //////////////
    //double muphi = 2.;
    //double sigmaphi = 1.5;
    //double mupi = 3.;
    //double sigmapi = 2.;
    //turbtherm.init_allnormal(muphi, sigmaphi, mupi, sigmapi);
    //std::cout << "normal init: " << turbtherm.getEnergy() << std::endl;
    //turbtherm.initEvolution();
    //std::cout << "after init step: " << turbtherm.getEnergy() << std::endl;
    //turbtherm.EvolveLattice();
    //std::cout << "after first evolution: " << turbtherm.getEnergy() << std::endl;
    //for(int64_t step=0; step<Nsteps; step += 1){ //two timesteps for one loop since measuring takes two operations 
    //    turbtherm.EvolveLattice();
    //    if (step%100==0) {std::cout << "step" << step << "/" << Nsteps << ": " << turbtherm.getEnergy() << std::endl;}
    //}
//////// 1. TEST: end /////////////////////////////////////////////////////////////////////////////////////////////////
//////// 2. TEST: init phi and pi deterministically without fluctuations //////////////////////////////////////////////
    //double phizeroth=5.;
    //double phiother=1.;
    //turbtherm.init_alldet(phizeroth, phiother);
//////// 2. TEST: end //////////////////////////////////////////////////////////////////////////////////////////////////

    //
    // Evolve
    //
    turbtherm.initEvolution();

    for(int64_t step=0; step<Nsteps; step += 2){ //two timesteps for one loop since measuring takes two operations 
        std::cout << "step:" << step << "/" << Nsteps << std::endl;
        if (step%100==0) {
            std::cout << "FFT and output..." << std::endl;
            turbtherm.fft_forward_phi(); //get ftphi
            turbtherm.copyftphi_to_ftphi_oldtimeslice(); //write to oldslice
            turbtherm.EvolveLattice();
            turbtherm.fft_forward_phi(); //get ftphi at new slice
            turbtherm.calcEnergypermode(); // needs ftphi and ftphi_oldtimeslize 
            // Write to file
            turbtherm.WriteOutput(step*MyLatticeParms.dt);
            //evolve again
            turbtherm.EvolveLattice();
        }
        else {
        turbtherm.EvolveLattice();
        turbtherm.EvolveLattice();
        }
    }

    return 0;
}
