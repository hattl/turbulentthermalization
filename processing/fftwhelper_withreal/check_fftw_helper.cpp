// g++ check_fftw_helper.cpp fftw_helper.cpp -o run.check_fftw_helper -lfftw3 -lm && ./run.check_fftw_helper
#include "fftw_helper.h"
#include <iostream>
#include <iomanip>

#include <fftw3.h>
#include <random>
#define REAL 0
#define IMAG 1

int main(int argc, char** argv)
{
    int64_t Nx=4;
    int d=2;
/////////////////////////////////////////////////////////////////////////////////////////////////
    std::cout << "D=" <<  d << std::endl; 
    std::cout << "**********************************" << std::endl; 
    std::cout << "Implementation of complex helper: " << std::endl; 
    fftw_helper helper_c("complex", Nx, d);
    //fftw_helper helper("complex", Nx, d, 10); // sets a max value for the normsquard to consider

    for (int lev=0; lev<helper_c.getnrlevels(); lev++){
        std::cout << "nr: " << lev << " value: "<< helper_c.getlevel(lev) << " degeneracy: " << helper_c.getdegeneracy(lev) << " fftw indices: ";
        for (int deg=0; deg<helper_c.getdegeneracy(lev); deg++){
                std::cout << helper_c.getindicesperlevel(lev,deg) << " ";
        }
        std::cout << std::endl; 
    }
/////////////////////////////////////////////////////////////////////////////////////////////////
    std::cout << "******************************" << std::endl; 
    std::cout << "Implementation of real helper: " << std::endl; 
    fftw_helper helper_r("real", Nx, d);
    //fftw_helper helper("complex", Nx, d, 10); // sets a max value for the normsquard to consider

    for (int lev=0; lev<helper_r.getnrlevels(); lev++){
        std::cout << "nr: " << lev << " value: "<< helper_r.getlevel(lev) << " degeneracy: " << helper_r.getdegeneracy(lev);
        std::cout << " nynquist: " << helper_r.isnynquist(lev) << " indices:";
        for (int deg=0; deg<helper_r.getdegeneracy(lev); deg++){
                std::cout << helper_r.getindicesperlevel(lev,deg) << " ";
        }
        std::cout << std::endl; 
    }
/////////////////////////////////////////////////////////////////////////////////////////////////
    std::cout << "******************************" << std::endl; 
    std::cout << "Compare both with fftw: " << std::endl; 
    //complex fft
    fftw_complex *signal_complex;
    fftw_complex *result_complex;
    signal_complex = (fftw_complex*) fftw_malloc( (int)std::pow(Nx,d) * sizeof(fftw_complex));
    result_complex = (fftw_complex*) fftw_malloc( (int)std::pow(Nx,d) * sizeof(fftw_complex));
    
    int n[d];
    for (int i = 0; i < d; ++i) {
        n[i]=Nx;
    }
    fftw_plan plan_complex = fftw_plan_dft( d, n, signal_complex, result_complex, FFTW_FORWARD, FFTW_ESTIMATE);

    // obtain signal
    for (int i = 0; i < (int)std::pow(Nx,d); ++i) {
        double theta = (double)i / (double)Nx * M_PI;
        signal_complex[i][REAL] = 1.0 * cos(10.0 * theta) +
                                  0.5 * cos(25.0 * theta);
        signal_complex[i][IMAG] = 0.;
    }

    fftw_execute(plan_complex);
    fftw_destroy_plan(plan_complex);
    
    //real fft
    double *signal_real;
    fftw_complex *result_real;
    signal_real = new double[(int)std::pow(Nx,d)];
    result_real = (fftw_complex*) fftw_malloc( ((int)std::pow(Nx,d-1)*(Nx/2+1))* sizeof(fftw_complex));

    fftw_plan plan_real = fftw_plan_dft_r2c( d, n, signal_real, result_real, FFTW_ESTIMATE);

    // obtain signal
    for (int i = 0; i < (int)std::pow(Nx,d); ++i) {
        double theta = (double)i / (double)Nx * M_PI;
        signal_real[i] = 1.0 * cos(10.0 * theta) +
                         0.5 * cos(25.0 * theta);
    }

    fftw_execute(plan_real);
    fftw_destroy_plan(plan_real);

    int w=10;
    //compare both
    std::cout << "Compare fft values" << std::endl;
    std::cout << "    COMPLEX version                  REAL version" << std::endl;
    for (int lev=0; lev<helper_c.getnrlevels(); lev++) {
        std::cout << "level nr: " << lev << " level value: "<< helper_r.getlevel(lev) << std::endl;
        for (int deg=0; deg<helper_c.getdegeneracy(lev); deg++) {
            std::cout << std::setw(w) << result_complex[helper_c.getindicesperlevel(lev,deg)][REAL] << " +i" << std::setw(w) << result_complex[helper_c.getindicesperlevel(lev,deg)][IMAG] << "    ";
            if (deg<helper_r.getdegeneracy(lev)) {
                std::cout << std::setw(w) << result_real[helper_r.getindicesperlevel(lev,deg)][REAL] << " +i" << std::setw(w) << result_real[helper_r.getindicesperlevel(lev,deg)][IMAG]; 
            }
            std::cout << std::endl;
        }
    }
/////////////////////////////////////////////////////////////////////////////////////////////////
    std::cout << "******************************" << std::endl; 
    std::cout << "Transform real one back to check: " << std::endl; 
    //real fft
    double *result_real2;
    fftw_complex *signal_real2;
    result_real2 = new double[(int)std::pow(Nx,d)];
    signal_real2 = (fftw_complex*) fftw_malloc( ((int)std::pow(Nx,d-1)*(Nx/2+1))* sizeof(fftw_complex));

    fftw_plan plan_real2 = fftw_plan_dft_c2r( d, n, signal_real2, result_real2, FFTW_ESTIMATE);

    // copy previous ft result to new signal
    std::cout << "FT signal: " << std::endl; 
    for (int i = 0; i < ((int)std::pow(Nx,d-1)*(Nx/2+1)); ++i) {
        signal_real2[i][REAL] = result_real[i][REAL];
        signal_real2[i][IMAG] = result_real[i][IMAG];
        std::cout << "Pos: " << i << " " << signal_real2[i][REAL] << " " << signal_real2[i][IMAG] << std::endl;
    }

    //signal_real2[3][REAL]  = 1.;
    //signal_real2[3][IMAG]  = 1.;
    //signal_real2[9][REAL]  = 1.;
    //signal_real2[9][IMAG]  = -1.;

    fftw_execute(plan_real2);
    fftw_destroy_plan(plan_real2);
   
    // check original signal with 2 times transformed one (scaled) 
    std::cout << "Comparison of original signal and two times transformed one: " << std::endl; 
    for (int i = 0; i < (int)std::pow(Nx,d); ++i) {
        std::cout << signal_real[i] << " " << result_real2[i]/std::pow(Nx,d) << std::endl;
    }
}
