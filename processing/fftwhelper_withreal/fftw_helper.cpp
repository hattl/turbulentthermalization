#include "fftw_helper.h"
#include <iostream>
#include <string>
#include <cmath>
#include <bits/stdc++.h>

int sortArr(int64_t *arr, int n, double maxvalarray, int64_t * &levels, int64_t * &degeneracy, int64_t ** &indicesperlevel )
{ 
    // Vector to store element 
    std::vector<std::pair<int, int>> vp; 
    std::vector<int> vlevels; 
    std::vector<int> vdegeneracy; 
  
    // Inserting element in pair vector 
    for (int i = 0; i < n; ++i) { 
        vp.push_back(std::make_pair(arr[i], i)); // value and index
    } 
  
    // Sorting pair vector 
    sort(vp.begin(), vp.end()); 

    // get unique values and count degeneracy 
    int tmp=-1; //start with neg value to get in else condition for fist loop
    for (int i=0; i < n; ++i) { // runs throug N**d
        if (vp[i].first==tmp) { //if value already exist, increase degeneracy
            vdegeneracy.back() += 1;
        }
        else {
            tmp=vp[i].first; 
            vlevels.push_back(tmp); 
            vdegeneracy.push_back(1); 
        }
    }

    // count number of levels smaller than maxvalarray
    int count = 0;
    for (int i=0; i < vlevels.size(); ++i) { 
        if (vlevels[i] <= maxvalarray) {
            count += 1;
        }
    }

    // write result to c arrays
    indicesperlevel = new int64_t*[count];
    levels = new int64_t[count];
    degeneracy = new int64_t[count];
    tmp=0;
    for (int i=0; i < count; ++i) { 
        indicesperlevel[i] = new int64_t[vdegeneracy[i]];
        levels[i]=vlevels[i];
        degeneracy[i]=vdegeneracy[i];
        for (int deg=0; deg < vdegeneracy[i]; ++deg) { 
            indicesperlevel[i][deg] = vp[tmp + deg].second;
        } 
        tmp += vdegeneracy[i];
    } 

    //std::cout << "LEVELS DEGENERACY INDICES" << std::endl ;
    //for (int i = 0; i < vlevels.size(); ++i) {
    //    std::cout << vlevels[i] << " " << vdegeneracy[i] << ": " ;
    //    for (int j=0; j < vdegeneracy[i]; ++j) { 
    //        std::cout << indicesperlevel[i][j] << " ";
    //    } 
    //    std::cout << std::endl;
    //} 
    return count; //number of levels smaller than maxlevelarrya
} 

//fftw_helper::fftw_helper(const char *fieldtype, int64_t Nx, int sdim, double max)
fftw_helper::fftw_helper(std::string fieldtype, int64_t Nx, int sdim, double max)
{
    // lattice parameter
    this->Nx=Nx;
    this->sdim=sdim;
    this->normvalsquaredmax=max;
    init();
}


//fftw_helper::fftw_helper(const char *fieldtype, int64_t Nx, int sdim)
fftw_helper::fftw_helper(std::string fieldtype, int64_t Nx, int sdim)
{
    // lattice parameter
    this->fieldtype=fieldtype;
    this->Nx=Nx;
    this->sdim=sdim;
    this->normvalsquaredmax=(Nx/2.)*(Nx/2.)*sdim; //consider normvalssquared smaller or equal to this value, dim*(N/2)**2 is not a restriction
    init();
} 

void fftw_helper::init() {
    // allocate nvalues depending on real or complex fftw
    if (fieldtype!="real" and fieldtype!="complex") std::cout << "ERROR: fieldtype must be real or complex" << std::endl;

    // fftw_odering 0,1,2,...,N/2,..,-N/2+1,...,-2,-1
    nvalues = new int64_t[Nx];
    // init
    for (int i = 0; i < Nx; ++i) {
        if (i>Nx/2) nvalues[i]=-(Nx-i);
        else nvalues[i]=i;
    }
    // fftw_odering for real fft 0,1,2,...,N/2 - last component of real fft
    nvalues_reallast = new int64_t[Nx/2+1];
    // init
    for (int i = 0; i < Nx/2+1; ++i) {
        nvalues_reallast[i]=i;
    }

    int64_t coordinate;
    if (fieldtype=="complex") {
        ftvolume = int(std::pow(Nx,sdim));
        // calculate n2 = nx2 + ny2 + ...
        normvalsquared = new int64_t[ ftvolume ];
        std::cout << "print n2 matrix from fftw (corr. index): " << std::endl;
        for (int i = 0; i < ftvolume; ++i) {
            std::cout << i << ": (";
            normvalsquared[i]=0;
            for (int d = 0; d < sdim; ++d) {
                coordinate = i/((int)std::pow(Nx,d)) % Nx;
                normvalsquared[i] += nvalues[coordinate]*nvalues[coordinate]; // calc of n2 = nx2 + ny2 +....
                std::cout << nvalues[coordinate] << ", ";
            }
            std::cout << ") ";
            std::cout << normvalsquared[i] << std::endl;
        }
    }

    if (fieldtype=="real") {
        std::cout << "Implementation faulty, check code...: " << std::endl;
        std::cout << "Output of realFFT stores some value twice (conjugated)" << std::endl;
        std::cout << "Find out by looking at degeneracy: this must be an even number; if it is odd, then there is a dublicate" << std::endl;
        ftvolume = int(std::pow(Nx,sdim-1)*(Nx/2+1)); // last coordinate is truncated
        // calculate n2 = nx2 + ny2 + ...
        normvalsquared = new int64_t[ftvolume];
        std::cout << "print n2 matrix from fftw (corr. index): " << std::endl;
        for (int i = 0; i < ftvolume; ++i) {
            std::cout << i << ": (";
            normvalsquared[i]=0;
            for (int d = 0; d < sdim-1; ++d) {
                coordinate = i/((int)std::pow(Nx,d)*(Nx/2+1)) % Nx;
                normvalsquared[i] += nvalues[coordinate]*nvalues[coordinate]; // calc of n2 = nx2 + ny2 +....
                std::cout << nvalues[coordinate] << ", ";
            }
            std::cout << nvalues_reallast[i % (Nx/2+1)] << ") ";
            //normvalsquared[i] += nvalues_reallast[i/((int)std::pow(Nx,sdim-1)) % (Nx/2+1)]*nvalues_reallast[i/((int)std::pow(Nx,sdim-1)) % (Nx/2+1)]; // last coordinate is special.
            normvalsquared[i] += nvalues_reallast[i % (Nx/2+1)]*nvalues_reallast[i % (Nx/2+1)]; // last coordinate is special.
            std::cout << normvalsquared[i] << std::endl;
        }
    }

    // get levels, degeneracy and indices per level for normvalsqueaed up to normvalsquaredmax  
    //this->nrlevels = sortArr( normvalsquared, int(std::pow(Nx,sdim)), sdim*(Nx/2)*(Nx/2), levels, degeneracy, indicesperlevel); 
    this->nrlevels = sortArr( normvalsquared, ftvolume, sdim*(Nx/2)*(Nx/2), levels, degeneracy, indicesperlevel); 

    // calculate nynquist frequencies and at wich level position they occour
    nynquistlevels = new int64_t[sdim+1];
    nynquistlevelspos = new int64_t[sdim+1];
    for (int d=0; d<sdim+1; d++){
        nynquistlevels[d]=d*(Nx/2)*(Nx/2);
        for (int lev=0; lev<nrlevels; lev++){
            if (nynquistlevels[d]==levels[lev]) nynquistlevelspos[d] = lev;
        }
    }

//    for (int lev = 0; lev < nrlevels; ++lev) {
//        std::cout << levels[lev] << " " << degeneracy[lev] << ": " ;
//        for (int deg=0; deg < degeneracy[lev]; ++deg) { 
//            std::cout << indicesperlevel[lev][deg] << " ";
//        } 
//        std::cout << std::endl;
//    }    


//    // print levels, degeneracy and indices per level
//    std::cout << "LEVELS DEGENERACY INDICES" << std::endl ;
//    for (int lev = 0; lev < nrlevels; ++lev) {
//        std::cout << levels[lev] << " " << degeneracy[lev] << ": " ;
//        for (int deg=0; deg < degeneracy[lev]; ++deg) { 
//            std::cout << indicesperlevel[lev][deg] << " ";
//        } 
//        std::cout << std::endl;
//    }    
//

}

int64_t fftw_helper::getnrlevels() {return nrlevels;}
int64_t fftw_helper::getlevel(int64_t i) {return levels[i];}
int64_t fftw_helper::getdegeneracy(int64_t i) {return degeneracy[i];}
int64_t fftw_helper::getindicesperlevel(int64_t i, int64_t deg) {return indicesperlevel[i][deg];}

int64_t fftw_helper::getlevelofpos(int64_t i) {return normvalsquared[i];}
bool    fftw_helper::isnynquist(int64_t i) {
    for (int d=0; d<sdim+1; d++){
        if (i==nynquistlevelspos[d]) return true;
    }
    return false;}
