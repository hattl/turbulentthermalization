import numpy as np
import matplotlib.pyplot as plt
import os
import sys

#files = ["densityofstates/densityofstates_D1_100.txt", "densityofstates/densityofstates_D2_100.txt", "densityofstates/densityofstates_D3_100.txt"]
files = ["densityofstates/densityofstates_D1_500.txt", "densityofstates/densityofstates_D2_500.txt", "densityofstates/densityofstates_D3_500.txt"]
colors = ["r","b","g"]
N = 200 # n2 range for chunks

for i, file in enumerate(files):
    if i==0:
        f = open(file)
        latticesize = int(f.readline().strip("#"))
    x, y = np.loadtxt(file, delimiter=' ', usecols=(0, 1), unpack=True)
    meany = np.mean(y)
    y = y/meany
    #x, y = np.loadtxt(file, delimiter=' ')
    plt.plot( x, y, label= str(i+1) + 'D', alpha=0.2, color=colors[i])
    # now split
    xsmoth = []
    xtmp = []
    ysmoth = []
    ytmp = []
    cnt = N
    for j in range(x.size):
        if x[j]<cnt:
            xtmp.append(x[j])
            ytmp.append(y[j])
        else:
            # finish earlier steo
            xsmoth.append( np.mean( xtmp) )
            ysmoth.append( np.mean( ytmp) )
            # clear tmp
            xtmp = []
            ytmp = []
            # update counter
            cnt += N
            # append current points
            xtmp.append(x[j])
            ytmp.append(y[j])
    plt.plot( xsmoth, ysmoth, alpha=0.8, color=colors[i])

plt.axvline(x=(latticesize/2)**2, label=r'$(\frac{N}{2})^2$', color="black")
plt.style.use('classic')
plt.title('Density of states on d dimensional lattice, N=' + str(latticesize))
plt.legend()
#plt.xscale('log')
plt.gca().set_ylim(bottom=0)
plt.gca().set_xlim(left=0)
#plt.xlim( [1/max(tmpdf.b), 1/min(tmpdf.b)])
#plt.ylim( [-0.1,1.1] )
plt.gcf().subplots_adjust(bottom=0.15)
plt.xlabel(r'$n^2=\sum n^2_i \propto E$')
plt.ylabel('degeneracy')
plt.savefig('densityofstates/Densityofstates_' + str(latticesize) + '.png')
