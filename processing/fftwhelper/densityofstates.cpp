// g++ densityofstates.cpp fftw_helper.cpp -o run.densityofstates && ./run.densityofstates
#include "fftw_helper.h"
#include <fstream>
#include <string>

int main(int argc, char** argv)
{
    int64_t Nx=100;
    int64_t levels;
    int64_t energy;
    int64_t degeneracy;

  
    for (int d=1; d<4; d++){
        std::cout << "D=" <<  d << std::endl; 
        std::ofstream myfile("densityofstates/densityofstates_D" + std::to_string(d) + "_" + std::to_string(Nx) +".txt"); 
        fftw_helper helper(Nx, d);
        levels=helper.getnrlevels();
        myfile << "# " << Nx << std::endl;
        for (int i=1; i<levels; i++){
            energy = helper.getlevel(i);
            degeneracy = helper.getdegeneracy(i);
            myfile << energy << " " << degeneracy << std::endl;
        }
        myfile.close();
    }

}
