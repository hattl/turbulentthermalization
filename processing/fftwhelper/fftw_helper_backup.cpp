#include "fftw_helper.h"
#include <iostream>
#include <cmath>
#include <bits/stdc++.h>

int sortArr(int64_t *arr, int n, double maxvalarray, int64_t * &levels, int64_t * &degeneracy, int64_t ** &indicesperlevel )
{ 
    // Vector to store element 
    std::vector<std::pair<int, int>> vp; 
    std::vector<int> vlevels; 
    std::vector<int> vdegeneracy; 
  
    // Inserting element in pair vector 
    for (int i = 0; i < n; ++i) { 
        vp.push_back(std::make_pair(arr[i], i)); // value and index
    } 
  
    // Sorting pair vector 
    sort(vp.begin(), vp.end()); 

    // get unique values and count degeneracy 
    int tmp=-1; //start with neg value to get in else condition for fist loop
    for (int i=1; i < n; ++i) { // runs throug N**d, forget about 0,0,0 state
        if (vp[i].first==tmp) { //if value already exist, increase degeneracy
            vdegeneracy.back() += 1;
        }
        else {
            tmp=vp[i].first; 
            vlevels.push_back(tmp); 
            vdegeneracy.push_back(1); 
        }
    }

    // count number of levels smaller than maxvalarray
    int count = 0;
    for (int i=0; i < vlevels.size(); ++i) { 
        if (vlevels[i] <= maxvalarray) {
            count += 1;
        }
    }

    // write result to c arrays
    indicesperlevel = new int64_t*[count];
    levels = new int64_t[count];
    degeneracy = new int64_t[count];
    tmp=0;
    for (int i=0; i < count; ++i) { 
        indicesperlevel[i] = new int64_t[vdegeneracy[i]];
        levels[i]=vlevels[i];
        degeneracy[i]=vdegeneracy[i];
        for (int deg=0; deg < vdegeneracy[i]; ++deg) { 
            indicesperlevel[i][deg] = vp[1 + tmp + deg].second;
        } 
        tmp += vdegeneracy[i];
    } 

    //std::cout << "LEVELS DEGENERACY INDICES" << std::endl ;
    //for (int i = 0; i < vlevels.size(); ++i) {
    //    std::cout << vlevels[i] << " " << vdegeneracy[i] << ": " ;
    //    for (int j=0; j < vdegeneracy[i]; ++j) { 
    //        std::cout << indicesperlevel[i][j] << " ";
    //    } 
    //    std::cout << std::endl;
    //} 
    return count; //number of levels smaller than maxlevelarrya
} 

fftw_helper::fftw_helper(int64_t Nx, int sdim, double max)
{
    // lattice parameter
    this->Nx=Nx;
    this->sdim=sdim;
    this->normvalsquaredmax=max;
    init();
}


fftw_helper::fftw_helper(int64_t Nx, int sdim)
{
    // lattice parameter
    this->Nx=Nx;
    this->sdim=sdim;
    this->normvalsquaredmax=(Nx/2.)*(Nx/2.)*sdim; //consider normvalssquared smaller or equal to this value, dim*(N/2)**2 is not a restriction
    init();
} 

void fftw_helper::init() {
    // allocate nvalues to fftw_odering 0,1,2,...,N/2,..,-N/2+1,...,-2,-1
    nvalues = new int64_t[Nx];
    // init
    for (int i = 0; i < Nx; ++i) {
        if (i>Nx/2) nvalues[i]=-(Nx-i);
        else nvalues[i]=i;
    }

//    // print fftw ordering
//    std::cout << std::endl;
//    std::cout << "fftw ordering: " << std::endl;
//    for (int i = 0; i < Nx; ++i) std::cout << nvalues[i];
//    std::cout << std::endl;

    // calculate n2 = nx2 + ny2 + ...
    normvalsquared = new int64_t[int(std::pow(Nx,sdim))];
    for (int i = 0; i < int(std::pow(Nx,sdim)); ++i) {
        normvalsquared[i]=0;
        for (int d = 0; d < sdim; ++d) {
            normvalsquared[i] += nvalues[i/((int)std::pow(Nx,d)) % Nx]*nvalues[i/((int)std::pow(Nx,d)) % Nx]; // calc of n2 = nx2 + ny2 +....
        }
    }

//    // print normvalssquared of fftw ordering - works only for 2dimension
//    std::cout << "print n2 matrix from fftw: " << std::endl;
//    for (int i = 0; i < Nx; ++i) {
//        std::cout << i << ": ";
//        for (int j = 0; j < Nx; ++j) {
//            std::cout << std::setw(4) << normvalsquared[i*Nx + j]  << " ";
//        }
//        std::cout << std::endl;
//    }

    // get levels, degeneracy and indices per level for normvalsqueaed up to normvalsquaredmax  
    this->nrlevels = sortArr( normvalsquared, int(std::pow(Nx,sdim)), normvalsquaredmax, levels, degeneracy, indicesperlevel); 

//    // print levels, degeneracy and indices per level
//    std::cout << "LEVELS DEGENERACY INDICES" << std::endl ;
//    for (int lev = 0; lev < nrlevels; ++lev) {
//        std::cout << levels[lev] << " " << degeneracy[lev] << ": " ;
//        for (int deg=0; deg < degeneracy[lev]; ++deg) { 
//            std::cout << indicesperlevel[lev][deg] << " ";
//        } 
//        std::cout << std::endl;
//    }    
}

int64_t fftw_helper::getnrlevels() {return nrlevels;}
int64_t fftw_helper::getlevel(int64_t i) {return levels[i];}
int64_t fftw_helper::getdegeneracy(int64_t i) {return degeneracy[i];}
int64_t fftw_helper::getindicesperlevel(int level, int deg) {return indicesperlevel[level][deg];}
