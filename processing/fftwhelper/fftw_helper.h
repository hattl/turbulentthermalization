#include <iostream>

class fftw_helper
{
  public:
    fftw_helper(int64_t Nx, int sdim); // constructor
    fftw_helper(int64_t Nx, int sdim, double max); // constructor
    int64_t getnrlevels(); //number of energy levels
    int64_t getlevel(int64_t i); //get level value of ith level
    int64_t getdegeneracy(int64_t i); //get degeneracy of ith level
    int64_t getindicesperlevel(int64_t i, int64_t deg); //get index for ith level and deg th degeneracy 

    bool isnynquist(int64_t i); //if level is nynquist frequency 

  private:
    // lattice parameter
    //const char *fieldtype; // complex or real
    int sdim; // dimension
    int64_t Nx; // sites per dimension
    int64_t *nvalues; // obtain n value from array index e.g. 0,1,2,3,-2,-1 
    int64_t *normvalsquared; // normvalues of fftw array
    int64_t normvalsquaredmax; // max value of normvalues to consider
    int64_t ftvolume; // size of the ft field

    int64_t nrlevels; //nr of energy levels
    int64_t *levels; // array of energy levels
    int64_t *degeneracy; // array of degeneracy per energy level
    int64_t **indicesperlevel; // [i][j]..j indices of fftw array with energy level nr i

    void init();

    int64_t getlevelofpos(int64_t i);
};
