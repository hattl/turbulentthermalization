//////////////////////////////////////////////////////////////////////////////
// Sets up indices for neighbors for a lattice with Nx sites per dim dimension
// 
// Note: dim in getUp(pos, dim) ranges from 0 to dim-1;
//      but for other values getUp will give some value back without complaint 
//////////////////////////////////////////////////////////////////////////////
#include <string>

class neighbor
{
  public:
    neighbor(int64_t Nx, int dim, std::string ordering); // constructor
    int64_t getUp(int64_t pos, int dim);
    int64_t getDn(int64_t pos, int dim);

  private:
    int64_t **upneighbor;
    int64_t **dnneighbor;
};
