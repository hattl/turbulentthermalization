//=====================================================================================//
// Helper for finding neigbors in a d dimensional cubic lattice encoded in 1d array
// implementation for c and fortran order:
//  - f order: [x][y][z], x is fastest index, IDX = x + y*N + z*N^2
//  - c order: [x][y][z], z is fastest index, IDX = z + y*N + x*N^2
//  Note: effectively they interchange dimension label
//=====================================================================================//

#include "neighbor.h"
#include <iostream>
#include <cmath>
#include <string>

// fortran
int *totuple_f (int n, int N, int dim)
{
    int *tup = new int[dim];

    for ( int i = 0; i < dim; i++ ) {
        tup[i] =  n/((int)std::pow(N,i)) % N;
    }
    return tup;
}

int tonumb_f (int *tup, int N, int dim)
{
    int number = 0;
    for ( int i = 0; i < dim; i++ ) {
        number += tup[i] * std::pow(N,i);
    }
    return number;
}

// c
int *totuple_c (int n, int N, int dim)
{
    int *tup = new int[dim];

    for ( int i = 0; i < dim; i++ ) {
        tup[i] =  n/((int)std::pow(N,dim-1-i)) % N;
    }
    return tup;
}

int tonumb_c (int *tup, int N, int dim)
{
    int number = 0;
    for ( int i = 0; i < dim; i++ ) {
        number += tup[i] * std::pow(N,dim-1-i);
    }
    return number;
}


neighbor::neighbor(int64_t Nx, int dim, std::string ordering)
{

    if (ordering!="f" and ordering!="c") std::cout << "ERROR: ordering must be c or f (fortran)" << std::endl;

    // init neigbors
    upneighbor = new int64_t*[int(pow(Nx,dim))];
    dnneighbor = new int64_t*[int(pow(Nx,dim))];
    for(int64_t pos=0;pos<int(pow(Nx,dim));pos++){
        upneighbor[pos] = new int64_t[dim];
        dnneighbor[pos] = new int64_t[dim];
    }

    // fortran ordering
    // f[x][y][z], x is the fastest, INDX = x + y N + z N**2
    if (ordering=="f"){
        // precalc neigbors
        int *tuple = new int[dim]; // tuple for current position
        int *tmptuple = new int[dim]; // tmp tuple for x,y storage
        for(int64_t pos=0;pos<int(pow(Nx,dim));pos++){
            tuple = totuple_f(pos, Nx, dim); // lenght = dim
            for (int j =0 ; j<dim; j++){
                for (int l =0 ; l<dim; l++) {tmptuple[l] = tuple[l];} // copy the current pos, since only one coordinate needs to be adopted
                // up
                tmptuple[j] = (tuple[j]+1) % Nx; // up (per dimension) 
                upneighbor[pos][j] = tonumb_f(tmptuple, Nx, dim); // site * (nr neigbors) + curr.neigbor
                // down
                tmptuple[j] = (tuple[j]-1+Nx) % Nx; // down (per dimension) 
                dnneighbor[pos][j] = tonumb_f(tmptuple, Nx, dim);  // site * (nr neigbors) + (curr.neigbor + dim)
            }
        }
    }
    // c ordering
    // f[x][y][z], z is the fastest, INDX = z + y N + x N**2
    if (ordering=="c"){
        // precalc neigbors
        int *tuple = new int[dim]; // tuple for current position
        int *tmptuple = new int[dim]; // tmp tuple for x,y storage
        for(int64_t pos=0;pos<int(pow(Nx,dim));pos++){
            tuple = totuple_c(pos, Nx, dim); // lenght = dim
            for (int j =0 ; j<dim; j++){
                for (int l =0 ; l<dim; l++) {tmptuple[l] = tuple[l];} // copy the current pos, since only one coordinate needs to be adopted
                // up
                tmptuple[j] = (tuple[j]+1) % Nx; // up (per dimension) 
                upneighbor[pos][j] = tonumb_c(tmptuple, Nx, dim); // site * (nr neigbors) + curr.neigbor
                // down
                tmptuple[j] = (tuple[j]-1+Nx) % Nx; // down (per dimension) 
                dnneighbor[pos][j] = tonumb_c(tmptuple, Nx, dim);  // site * (nr neigbors) + (curr.neigbor + dim)
            }
        }
    }
}

int64_t neighbor::getUp(int64_t pos, int dim) {return upneighbor[pos][dim];}
int64_t neighbor::getDn(int64_t pos, int dim) {return dnneighbor[pos][dim];} 
